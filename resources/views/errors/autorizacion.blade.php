@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger fade in text-center">
                    <h4 class="text-bold"><i class="fa fa-warning text-warning"></i> No tienes acceso al modulo</h4>
                </div>
            </div>
        </div>
    </div>
@endsection