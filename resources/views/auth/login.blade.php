@extends('layouts.login')

@section('content')
    <!-- /.login-logo -->
    <div class="login-box-body">
        <form method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            <div class="form-group has-feedback {{ $errors->has('username') ? ' has-error' : '' }}">
                <input id="username" type="text" class="form-control" placeholder="Usuario" name="username" value="{{ old('username') }}">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
                <input id="password" type="password" class="form-control" placeholder="Contraseña" name="password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            @if ($errors->has('username'))
                <p class="text-danger text-center">
                    <strong><i class="fa fa-warning"></i> {{ $errors->first('username') }}</strong>
                </p>
            @endif
            @if ($errors->has('password'))
                <p class="text-danger text-center">
                    <strong><i class="fa fa-warning"></i> {{ $errors->first('password') }}</strong>
                </p>
            @endif
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Matenerme Logueado
                            </label>
                        </div>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-sign-in"></i> Ingresar</button>
                </div>
                <!-- /.col -->
            </div>
        </form>
    </div>
    <!-- /.login-box-body -->
@endsection
