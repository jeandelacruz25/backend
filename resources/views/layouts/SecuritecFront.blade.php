<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Rentas Securitec Perú</title>
        <!-- Favicon -->
        <link rel="shortcut icon" href="favicon.png" type="image/x-icon">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Theme style -->
        <link rel="stylesheet" type="text/css" href="{!! asset('css/app.css')!!}">
        <link rel="stylesheet" type="text/css" href="{!! asset('css/adminlte.css')!!}">
        <link rel="stylesheet" type="text/css" href="{!! asset('css/securitec.css')!!}">
        {!! Charts::styles(['chartjs']) !!}
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition skin-red sidebar-mini">
        <div id="securitecFront" class="wrapper">
            @include('layouts.recursos.header.index')
            @include('layouts.recursos.leftmenu.index')
            <div class="content-wrapper">
                <section class="content-header">
                    <h1>
                        Securitec
                        <small>Version 1.0</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li class="active"><i class="fa fa-dashboard"></i> Hola, {{ Auth::user()->name }}</li>
                    </ol>
                </section>
                <section class="content">
                    @yield('content')
                </section>
            </div>
            @include('layouts.recursos.footer')
            @include('layouts.recursos.rightmenu')
            @include('layouts.recursos.modals')
        </div>
        <script src="{!! asset('js/app.js?version='.date('YmdHis'))!!}"></script>
        <script src="{!! asset('js/adminlte.js?version='.date('YmdHis'))!!}"></script>
        <script src="{!! asset('js/plugins.js?version='.date('YmdHis'))!!}"></script>
        <script src="{!! asset('js/helper.js?version='.date('YmdHis'))!!}"></script>
        <script src="{!! asset('js/vue/vueSecuritec.js?version='.date('YmdHis'))!!}"></script>
        {!! Charts::scripts(['chartjs']) !!}
        @yield('scripts')
    </body>
</html>
