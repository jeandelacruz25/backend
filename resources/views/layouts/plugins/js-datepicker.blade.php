<script src="{{ asset('plugins/daterangepicker.js')}}">  </script>
<script>
    $('.fecha_simple').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        alwaysShowCalendars: true,
        autoUpdateInput: true,
        opens: "right",
        locale: {
            format: 'YYYY-MM'
        }
    })
    $('.fecha_simple').on('apply.daterangepicker', function (ev, picker) {
        let startDate = picker.startDate;
        $('.fecha_simple').val(startDate.format('YYYY-MM'));
    })

    $('.fecha_rango').daterangepicker({
        locale: {
            format: 'YYYY-MM-DD'
        },
        opens: "center",
        ranges: {
            'Hoy': [moment(), moment()],
            'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Hace 7 dias': [moment().subtract(6, 'days'), moment()],
            'Hace 30 dias': [moment().subtract(29, 'days'), moment()],
            'Mes Actual': [moment().startOf('month'), moment().endOf('month')],
            'Mes Anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    })
</script>