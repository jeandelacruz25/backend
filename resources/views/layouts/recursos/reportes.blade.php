@include('layouts.plugins.css-datepicker')
@include('layouts.plugins.js-datepicker')
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Filtro {{ $titleFilterReport }}</h3>
            </div>
            <div class="box-body" style="">
                <div class="row">
                    @if($viewRangeDateAndMonth)
                        <div class="col-md-2">
                            <select id="selectFilterDate" class="form-control">
                                <option value="mesanio" selected>Mes / Año</option>
                                <option value="rango">Rango de Fechas</option>
                            </select>
                        </div>
                        <div id="mesanio" style="display: block;">
                            <div class="col-md-4">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <select id="selectedMonth" name="selectedMonth" class="form-control">
                                        @foreach ($optionsMonth as $month)
                                            <option value="{{ sprintf('%02d', $month['mes']) }}" {{ \Carbon\Carbon::now()->format('m') == $month['mes'] ? 'selected' : '' }}>{{ ucwords(mb_strtolower($month['DESC_LARGA'], 'UTF-8')) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar-check-o"></i>
                                    </div>
                                    <select id="selectedYear" name="selectedYear" class="form-control">
                                        @foreach ($optionsYear as $year)
                                            <option value="{{ $year['anio'] }}" {{ \Carbon\Carbon::now()->format('Y') == $year['anio'] ? 'selected' : '' }}>{{ $year['anio'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div id="rango" style="display:none">
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar-check-o"></i>
                                    </span>
                                    <input id="dateRange" type="text" name="dateRange" class="form-control fecha_rango" value="">
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($viewDateMonth)
                        <div class="col-md-{{ $viewTipoProducto ? 2 : 3 }}">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <select id="selectedMonth" name="selectedMonth" class="form-control">
                                    @foreach ($optionsMonth as $month)
                                        <option value="{{ sprintf('%02d', $month['mes']) }}" {{ \Carbon\Carbon::now()->format('m') == $month['mes'] ? 'selected' : '' }}>{{ ucwords(mb_strtolower($month['DESC_LARGA'], 'UTF-8')) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif
                    @if($viewDateYear)
                        <div class="col-md-{{ $viewTipoProducto ? 2 : 3 }}">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar-check-o"></i>
                                </div>
                                <select id="selectedYear" name="selectedYear" class="form-control">
                                    @foreach ($optionsYear as $year)
                                        <option value="{{ $year['anio'] }}" {{ \Carbon\Carbon::now()->format('Y') == $year['anio'] ? 'selected' : '' }}>{{ $year['anio'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif
                    @if($viewClientes)
                        <div class="col-md-{{ $viewTipoProducto || $viewDateMonth || $viewDateYear ? 3 : 6 }}">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                </div>
                                <select id="selectedClientes" name="selectedClientes" class="form-control">
                                    <option value="">Todos</option>
                                    @foreach ($optionsClientes as $clientes)
                                        <option value="{{ $clientes['id_cliente'] }}" @if($clientes['id_cliente'] == 1) selected @endif>{{ ucwords(mb_strtolower($clientes['cliente'], 'UTF-8')) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif
                    @if($viewProveedores)
                        <div class="col-md-{{ $viewTipoProducto || $viewDateMonth || $viewDateYear ? 3 : 6 }}">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                </div>
                                <select id="selectedProveedor" name="selectedProveedor" class="form-control">
                                    @foreach ($optionsProveedores as $proveedores)
                                        <option value="{{ $proveedores['id_proveedor'] }}">{{ ucwords(mb_strtolower($proveedores['proveedor'], 'UTF-8')) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif
                    @if($viewTipoProducto)
                        <div class="col-md-{{ $viewClientes || $viewProveedores || $viewDateMonth || $viewDateYear ? 3 : 6 }}">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-product-hunt"></i>
                                </div>
                                <select id="selectedTipoProducto" name="selectedTipoProducto" class="form-control">
                                    @foreach ($optionsTipoProducto as $tipoproducto)
                                        <option value="{{ $tipoproducto['id_tipoproducto'] }}" {{ $tipoproducto['id_tipoproducto'] === 2 ? 'selected' : '' }}>{{ ucwords(mb_strtolower($tipoproducto['tipo_producto'], 'UTF-8')) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif
                    <div class="col-md-{{ $viewTipoProducto || $viewDateMonth || $viewDateYear || $viewRangeDateAndMonth ? 2 : 6 }} text-center">
                        <a class="btn btn-success" onclick="loadDatatable()"><i class="fa fa-search"></i> Buscar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    selectMesAnioRange()
</script>