<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="img/user_default.png" class="img-circle" alt="{{ Auth::user()->name }}">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MENÚ PRINCIPAL</li>
            <li class="clientes">
                <a href="#" v-on:click="loadOptionMenu('clientes')"><i class="fa fa-group text-grey"></i> <span>Gestionar Clientes</span></a>
            </li>
            <li class="proveedores">
                <a href="#" v-on:click="loadOptionMenu('proveedores')"><i class="fa fa-bank text-aqua"></i> <span>Gestionar Proveedores</span></a>
            </li>
            <li class="treeview">
                <a href="#"><i class="fa fa-product-hunt text-primary"></i> <span>Gestionar Productos</span> <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu" style="display: none;">
                    <li class="tipoproductos"><a href="#" v-on:click="loadOptionMenu('tipoproductos')"><i class="fa fa-circle-o text-purple"></i> Tipo de Productos</a></li>
                    <li class="productos"><a href="#" v-on:click="loadOptionMenu('productos')"><i class="fa fa-circle-o text-aqua"></i> Lista de Productos</a></li>
                    <li class="productosclientes"><a href="#" v-on:click="loadOptionMenu('productosclientes')"><i class="fa fa-circle-o text-black"></i> Productos de Clientes</a></li>
                    <li class="productosproveedores"><a href="#" v-on:click="loadOptionMenu('productosproveedores')"><i class="fa fa-circle-o text-gray"></i> Productos de Proveedores</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class="fa fa-bar-chart text-green"></i> <span>Gestionar Graficos</span> <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu" style="display: none;">
                    <li class="costosproveedor"><a href="#" v-on:click="loadOptionMenu('costosproveedor')"><i class="fa fa-circle-o text-primary"></i> Costos Tel. por Proveedor</a></li>
                    <li class="costossmsproveedor"><a href="#" v-on:click="loadOptionMenu('costossmsproveedor')"><i class="fa fa-circle-o text-teal"></i> Costos SMS por Proveedor</a></li>
                    <li class="costodiariotelefoniaproveedor"><a href="#" v-on:click="loadOptionMenu('costodiariotelefoniaproveedor')"><i class="fa fa-circle-o text-purple"></i> Costo Diario Tel. Proveedor</a></li>
                    <li class="ventasclientes"><a href="#" v-on:click="loadOptionMenu('ventasclientes')"><i class="fa fa-circle-o text-success"></i> Ventas por Cliente</a></li>
                    <li class="rentabilidadproductocliente"><a href="#" v-on:click="loadOptionMenu('rentabilidadproductocliente')"><i class="fa fa-circle-o text-gray"></i> Ren. T.Producto y Cliente</a></li>
                    <li class="rentabilidadcliente"><a href="#" v-on:click="loadOptionMenu('rentabilidadcliente')"><i class="fa fa-circle-o text-green"></i> Rentabilidad Total Cliente</a></li>
                    <li class="rentabilidadtelefoniacliente"><a href="#" v-on:click="loadOptionMenu('rentabilidadtelefoniacliente')"><i class="fa fa-circle-o text-green"></i> Ren. Diaria Telefonia Cliente</a></li>
                    <li class="rentabilidadsmscliente"><a href="#" v-on:click="loadOptionMenu('rentabilidadsmscliente')"><i class="fa fa-circle-o text-fuchsia"></i> Ren. Diaria SMS Cliente</a></li>
                    <li class="rentabilidadanual"><a href="#" v-on:click="loadOptionMenu('rentabilidadanual')"><i class="fa fa-circle-o text-danger"></i> Rentabilidad Mensual</a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>