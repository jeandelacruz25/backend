<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; {{ Date::now()->format('Y') }} <a href="http://www.securitec.pe/">Securitec Perú</a>.</strong> Todos los Derechos Reservados.
</footer>