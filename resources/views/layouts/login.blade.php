<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.png" type="image/x-icon">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Theme style -->
    <link rel="stylesheet" type="text/css" href="{!! asset('css/app.css')!!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('css/adminlte.css')!!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('css/securitec.css')!!}">
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page" style="background: #232c3b !important">
    <div class="login-box">
        <div class="login-logo">
            <img class="img-push" src="{!! asset('img/logo.png') !!}">
        </div>
        @yield('content')
    </div>
    <!-- /.login-box -->
    <script src="{!! asset('js/app.js?version='.date('YmdHis'))!!}"></script>
    <script src="{!! asset('js/adminlte.js?version='.date('YmdHis'))!!}"></script>
    <script src="{!! asset('js/helper.js?version='.date('YmdHis'))!!}"></script>
    <script src="{!! asset('js/helper.js?version='.date('YmdHis'))!!}"></script>
    <script src="{!! asset('js/jquery.backstretch.js?version='.date('YmdHis'))!!}"></script>
    <script>
        $.backstretch([
            "img/wallpaper/wallpaper_1.jpg",
            "img/wallpaper/wallpaper_2.jpg"
        ], {duration: 3000, fade: 750});
    </script>
</body>
</html>
