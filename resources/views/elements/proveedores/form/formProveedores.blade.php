<!-- Modal content-->
<div class="panel panel-primary">
    <div class="panel-heading">
        <button type="button" class="close" onclick="clearModalClose('modalSecuritec', 'div.dialogSecuritec')" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ $updateForm === true ? "Editar" : "Agregar" }} Proveedor</h4>
    </div>
    <div class="modal-body">
        <form id="formProveedor">
            <div class="form-group">
                <label>Nombre Proveedor</label>
                <input type="text" name="nameProveedor" class="form-control" placeholder="Ingresar el nombre del proveedor" value="{{ $nameProveedor }}">
            </div>
            <div class="alert alert-danger formError" style="display: none"></div>
            <input type="hidden" name="proveedorID" value="{{ $idProveedor }}">
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btnForm">{!! $updateForm === true ? "<i class='fa fa-edit'></i> Editar" : "<i class='fa fa-plus'></i> Agregar" !!}</button>
                <button type="button" class="btn btn-info btnLoad" style="display: none"><i class="fa fa-spin fa-spinner"></i> Cargando</button>
                <button type="button" class="btn btn-default" onclick="clearModalClose('modalSecuritec', 'div.dialogSecuritec')" data-dismiss="modal"><i class="fa fa-close"></i> Cerrar</button>
            </div>
        </form>
    </div>
</div>
<script src="{!! asset('js/form/formProveedor.js?version='.date('YmdHis')) !!}"></script>
<script>
    hideErrorForm('.formError')
    clearModalClose('modalAsterisk', 'div.dialogAsterisk')
</script>