<!-- Modal content-->
<div class="panel panel-primary">
    <div class="panel-heading">
        <button type="button" class="close" onclick="clearModalClose('modalSecuritec', 'div.dialogSecuritec')" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ $updateForm == true ? "Editar" : "Agregar" }} Producto Cliente</h4>
    </div>
    <div class="modal-body">
        <form id="formProveedoresProductos">
            <div class="form-group">
                <label>Mes</label>
                <select id="selectedMonth" name="selectedMonth" class="form-control">
                    @foreach ($optionsMonth as $month)
                        <option value="{{ sprintf('%02d', $month['mes']) }}" {{ sprintf('%02d', $month['mes']) == $selectedMonth ? "selected" : "" }}>{{ $month['DESC_LARGA'] }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Año</label>
                <select id="selectedAnio" name="selectedAnio" class="form-control">
                    @foreach ($optionsYear as $year)
                        <option value="{{ $year['anio'] }}" {{ $year['anio'] == $selectedYear ? "selected" : "" }}>{{ $year['anio'] }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Proveedor</label>
                <select id="selectedProveedor" name="selectedProveedor" class="form-control">
                    @foreach ($optionsProveedores as $proveedor)
                        <option value="{{ $proveedor['id_proveedor'] }}" {{ $proveedor['id_proveedor'] == $selectedProveedores ? "selected" : "" }}>{{ $proveedor['proveedor'] }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Producto</label>
                <select id="selectedProducto" name="selectedProducto" class="form-control">
                    @foreach ($optionsProductos as $productos)
                        <option value="{{ $productos['id_producto'] }}" {{ $productos['id_producto'] == $selectedProductos ? "selected" : "" }}>{{ $productos['producto'] }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Cantidad</label>
                <input type="text" name="numCantidad" class="form-control" value="{{ $numCantidad }}">
            </div>
            <div class="form-group">
                <label>Precio Costo</label>
                <input type="text" name="numPrecioCosto" class="form-control" value="{{ $numPrecioCosto }}">
            </div>
            <div class="alert alert-danger formError" style="display: none"></div>
            <input type="hidden" name="productoProveedorID" value="{{ $idProductoProveedor }}">
            <input type="hidden" name="estadoID" value="{{ $idEstado ? $idEstado : 1 }}">
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btnForm">{!! $updateForm === true ? "<i class='fa fa-edit'></i> Editar" : "<i class='fa fa-plus'></i> Agregar" !!}</button>
                <button type="button" class="btn btn-info btnLoad" style="display: none"><i class="fa fa-spin fa-spinner"></i> Cargando</button>
                <button type="button" class="btn btn-default" onclick="clearModalClose('modalSecuritec', 'div.dialogSecuritec')" data-dismiss="modal"><i class="fa fa-close"></i> Cerrar</button>
            </div>
        </form>
    </div>
</div>
<script src="{!! asset('js/form/formProveedoresProductos.js?version='.date('YmdHis')) !!}"></script>
<script>
    hideErrorForm('.formError')
    clearModalClose('modalAsterisk', 'div.dialogAsterisk')
</script>