<!-- Modal content-->
<div class="panel panel-primary">
    <div class="panel-heading">
        <button type="button" class="close" onclick="clearModalClose('modalSecuritec', 'div.dialogSecuritec')" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ $updateForm == true ? "Editar" : "Agregar" }} Producto</h4>
    </div>
    <div class="modal-body">
        <form id="formProducto">
            <div class="form-group">
                <label>Nombre Producto</label>
                <input type="text" name="nameProducto" class="form-control" placeholder="Ingresar el nombre del producto" value="{{ $nameProducto }}">
            </div>
            <div class="form-group">
                <label>Tipo Producto</label>
                <select id="selectedTipoProducto" name="selectedTipoProducto" class="form-control">
                    @foreach ($optionsTipoProducto as $tipoProducto)
                        <option value="{{ $tipoProducto['id_tipoproducto'] }}" {{ $tipoProducto['id_tipoproducto'] == $selectedTipoProducto ? "selected" : "" }}>{{ $tipoProducto['tipo_producto'] }}</option>
                    @endforeach
                </select>
            </div>
            <div class="alert alert-danger formError" style="display: none"></div>
            <input type="hidden" name="productoID" value="{{ $idProducto }}">
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btnForm">{!! $updateForm === true ? "<i class='fa fa-edit'></i> Editar" : "<i class='fa fa-plus'></i> Agregar" !!}</button>
                <button type="button" class="btn btn-info btnLoad" style="display: none"><i class="fa fa-spin fa-spinner"></i> Cargando</button>
                <button type="button" class="btn btn-default" onclick="clearModalClose('modalSecuritec', 'div.dialogSecuritec')" data-dismiss="modal"><i class="fa fa-close"></i> Cerrar</button>
            </div>
        </form>
    </div>
</div>
<script src="{!! asset('js/form/formProductos.js?version='.date('YmdHis')) !!}"></script>
<script>
    hideErrorForm('.formError')
    clearModalClose('modalAsterisk', 'div.dialogAsterisk')
</script>