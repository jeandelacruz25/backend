<!-- Modal content-->
<div class="panel panel-primary">
    <div class="panel-heading">
        <button type="button" class="close" onclick="clearModalClose('modalSecuritec', 'div.dialogSecuritec')" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ $updateForm == true ? "Editar" : "Agregar" }} Producto Cliente</h4>
    </div>
    <div class="modal-body">
        <form id="formClientesProductos">
            <div class="form-group">
                <label>Mes</label>
                <select id="selectedMonth" name="selectedMonth" class="form-control">
                    @foreach ($optionsMonth as $month)
                        <option value="{{ sprintf('%02d', $month['mes']) }}" {{ sprintf('%02d', $month['mes']) == $selectedMonth ? "selected" : "" }}>{{ $month['DESC_LARGA'] }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Año</label>
                <select id="selectedAnio" name="selectedAnio" class="form-control">
                    @foreach ($optionsYear as $year)
                        <option value="{{ $year['anio'] }}" {{ $year['anio'] == $selectedYear ? "selected" : "" }}>{{ $year['anio'] }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Cliente</label>
                <select id="selectedCliente" name="selectedCliente" class="form-control">
                    @foreach ($optionsClientes as $clientes)
                        <option value="{{ $clientes['id_cliente'] }}" {{ $clientes['id_cliente'] == $selectedClientes ? "selected" : "" }}>{{ $clientes['cliente'] }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Producto</label>
                <select id="selectedProducto" name="selectedProducto" class="form-control">
                    @foreach ($optionsProductos as $productos)
                        <option value="{{ $productos['id_producto'] }}" {{ $productos['id_producto'] == $selectedProductos ? "selected" : "" }}>{{ $productos['producto'] }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Cantidad</label>
                <input type="text" name="numCantidad" class="form-control" value="{{ $numCantidad }}">
            </div>
            <div class="form-group">
                <label>Precio Venta</label>
                <input type="text" name="numPrecioVenta" class="form-control" value="{{ $numPrecioVenta }}">
            </div>
            <div class="alert alert-danger formError" style="display: none"></div>
            <input type="hidden" name="productoClienteID" value="{{ $idProductoCliente }}">
            <input type="hidden" name="estadoID" value="{{ $idEstado ? $idEstado : 1 }}">
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btnForm">{!! $updateForm === true ? "<i class='fa fa-edit'></i> Editar" : "<i class='fa fa-plus'></i> Agregar" !!}</button>
                <button type="button" class="btn btn-info btnLoad" style="display: none"><i class="fa fa-spin fa-spinner"></i> Cargando</button>
                <button type="button" class="btn btn-default" onclick="clearModalClose('modalSecuritec', 'div.dialogSecuritec')" data-dismiss="modal"><i class="fa fa-close"></i> Cerrar</button>
            </div>
        </form>
    </div>
</div>
<script src="{!! asset('js/form/formClientesProductos.js?version='.date('YmdHis')) !!}"></script>
<script>
    hideErrorForm('.formError')
    clearModalClose('modalAsterisk', 'div.dialogAsterisk')
</script>