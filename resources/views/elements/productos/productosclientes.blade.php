@if($filterReport) @include('layouts.recursos.reportes') @endif
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $titleModule }}</h3>
                <div class="box-tools">
                    <div class="btn-group pull-right">
                        <a onclick="responseModal('div.dialogSecuritec','formProductosClientes')" data-toggle="modal" data-target="#modalSecuritec" class="btn btn-primary"><i class="fa fa-product-hunt" aria-hidden="true" ></i> Agregar Productos Clientes</a>
                    </div>
                </div>
            </div>
            <div class="box-body" style="">
                <div class="row">
                    <div class="col-md-12">
                        <table id="listProductosClientes" class="table table-bordered" style="width: 100% !important">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Cliente</th>
                                <th>Producto</th>
                                <th>Cantidad</th>
                                <th>Precio Venta</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        loadDatatable()
    })
    function loadDatatable(){
        let parameters = {
            _token: $('meta[name="csrf-token"]').attr('content'),
            selectedClientes : $('#selectedClientes').val(),
            selectedMonth : $('#selectedMonth').val(),
            selectedYear : $('#selectedYear').val()
        }
        dataTables('listProductosClientes', '/listProductosClientes', parameters)
    }
</script>