<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $titleModule }}</h3>
                <div class="box-tools">
                    <div class="btn-group pull-right">
                        <a onclick="responseModal('div.dialogSecuritec','formProductos')" data-toggle="modal" data-target="#modalSecuritec" class="btn btn-primary"><i class="fa fa-product-hunt" aria-hidden="true" ></i> Agregar Producto</a>
                    </div>
                </div>
            </div>
            <div class="box-body" style="">
                <div class="row">
                    <div class="col-md-12">
                        <table id="listProductos" class="table table-bordered" style="width: 100% !important">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Producto</th>
                                <th>Tipo</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        loadDatatable()
    })
    function loadDatatable(){
        dataTables('listProductos', '/listProductos')
    }
</script>