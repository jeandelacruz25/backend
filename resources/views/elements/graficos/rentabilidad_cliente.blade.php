@if($filterReport) @include('layouts.recursos.reportes') @endif
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $titleModule }}</h3>
            </div>
            <div class="box-body" style="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#grafico" data-toggle="tab"><i class="fa fa-bar-chart"></i> Grafico</a></li>
                                <li><a href="#tabla" data-toggle="tab"><i class="fa fa-table"></i> Tabla Detallada</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="grafico">
                                    <div class="chartHTML"></div>
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="tabla">
                                    <div class="chartTable"></div>
                                </div>
                            </div>
                            <!-- /.tab-content -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="chartScript"></div>
<script>
    $(document).ready(function(){
        loadDatatable()
    })

    function loadDatatable(){
        axiosChart('get', '/getDataRentabilidadCliente', {
            selectedYear: $('#selectedYear').val(),
            selectedMonth: $('#selectedMonth').val()
        })
    }
</script>