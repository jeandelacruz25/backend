/**
 * Created by Carlos on 15/12/2017.
 *
 * [dataTables_lang_spanish description]
 * @return Retorna el idioma para el datatable
 */
const dataTables_lang_spanish = () => {
    let lang = {
        'sProcessing': 'Procesando...',
        'sLengthMenu': 'Mostrar _MENU_ registros',
        'sZeroRecords': 'No se encontraron resultados',
        'sEmptyTable': 'Ningún dato disponible en esta tabla',
        'sInfo': 'Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros',
        'sInfoEmpty': 'Mostrando registros del 0 al 0 de un total de 0 registros',
        'sInfoFiltered': '(filtrado de un total de _MAX_ registros)',
        'sInfoPostFix': '',
        'sSearch': 'Buscar:',
        'sUrl': '',
        'sInfoThousands': ',',
        'sLoadingRecords': 'Cargando...',
        'oPaginate': {
            'sFirst': 'Primero',
            'sLast': 'Último',
            'sNext': 'Siguiente',
            'sPrevious': 'Anterior'
        },
        'oAria': {
            'sSortAscending': ': Activar para ordenar la columna de manera ascendente',
            'sSortDescending': ': Activar para ordenar la columna de manera descendente'
        }
    }

    return lang
}

/**
 * Created by Carlos on 15/12/2017.
 *
 * [columnsDatatable description]
 * @routes El ID de la tabla
 * @return Devuelve las columnas a tomarse
 */
const columnsDatatable = (route) => {
    let columns = ''
    if (route === 'listClientes') {
        columns = [
            {data: 'id', name: 'id'},
            {data: 'cliente', name: 'cliente'},
            {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
        ]
    }

    if (route === 'listProductos') {
        columns = [
            {data: 'id', name: 'id'},
            {data: 'producto', name: 'producto'},
            {data: 'tipo_producto', name: 'tipo_producto'},
            {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
        ]
    }

    if (route === 'listTipoProductos') {
        columns = [
            {data: 'id', name: 'id'},
            {data: 'tipo_producto', name: 'tipo_producto'},
            {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
        ]
    }

    if (route === 'listProductosClientes') {
        columns = [
            {data: 'id', name: 'id'},
            {data: 'cliente', name: 'cliente'},
            {data: 'producto', name: 'producto'},
            {data: 'cantidad', name: 'cantidad'},
            {data: 'precio_venta', name: 'precio_venta'},
            {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
        ]
    }

    if (route === 'listProductosProveedores') {
        columns = [
            {data: 'id', name: 'id'},
            {data: 'proveedor', name: 'proveedor'},
            {data: 'producto', name: 'producto'},
            {data: 'cantidad', name: 'cantidad'},
            {data: 'precio_costo', name: 'precio_costo'},
            {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
        ]
    }

    if (route === 'listProveedores') {
        columns = [
            {data: 'id', name: 'id'},
            {data: 'proveedor', name: 'proveedor'},
            {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
        ]
    }
    return columns
}

/**
 * Created by Carlos on 15/12/2017.
 *
 * [dataTables description]
 * @nombreDIV ID de la tabla
 * @routes Ruta de donde se tomara los datos
 * @return Estructura el Datatable ah tomarse
 */
const dataTables = (nombreDIV, routes, parameters = {}) => {
    // Eliminación del DataTable en caso de que exista
    $(`#${nombreDIV}`).dataTable().fnDestroy()
    // Creacion del DataTable
    $(`#${nombreDIV}`).DataTable({
        'deferRender': true,
        'processing': true,
        'serverSide': true,
        'ajax': {
            url: routes,
            type: 'POST',
            data: parameters
        },
        'paging': true,
        'pageLength': 10,
        'lengthMenu': [10, 20, 40, 50, 100, 200, 300, 400, 500],
        'language': dataTables_lang_spanish(),
        'columns': columnsDatatable(nombreDIV)
    })
}