/**
 * Created by Carlos on 15/12/2017.
 */
const securitecFront = new Vue({
    el: '#securitecFront',
    data: {
        active: ''
    },
    mounted: function () {
        this.loadOptionMenu('clientes')
    },
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}) {
            try {
                let response = await axios.post(`${urlModule}`, parameters)
                return response.data
            } catch (error) { return error.status }
        },
        loadOptionMenu: async function (idMenu) {
            $('li').removeClass('active')
            $(`li.${idMenu}`).addClass('active')
            $('#container').html(`<div class="cssload-container"><div class="cssload-lt"></div><div class="cssload-rt"></div><div class="cssload-lb"></div><div class="cssload-rb"></div></div>`)
            $('#container').html(await this.sendUrlRequest(`/${idMenu}`))
        }
    }
})