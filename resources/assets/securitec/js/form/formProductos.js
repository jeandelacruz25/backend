/**
 * Created by Carlos on 15/12/2017.
 */
$('#formProducto').submit(function(e) {
    let data = $(this).serialize()
    changeButtonForm('btnForm','btnLoad')
    $.ajax({
        type        : 'POST',
        url         : 'saveformProductos',
        cache       : false,
        headers     : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        data        : data,
        success: function(data){
            if(data.message === 'Success'){
                changeButtonForm('btnLoad','btnForm')
                let action = (data.action === 'create' ? 'agrego' : 'edito')
                alertaSimple('', 'Se '+ action +' correctamente el producto', 'success', 2000)
                loadDatatable()
                clearModal('modalSecuritec', 'div.dialogSecuritec')
            }else{
                alertaSimple('', 'Problemas de inserción a la base de datos', 'error', 2000)
            }
            changeButtonForm('btnLoad','btnForm')
        },
        error : function(data) {
            changeButtonForm('btnLoad','btnForm')
            showErrorForm(data, '.formError')
        }
    })
    e.preventDefault()
})