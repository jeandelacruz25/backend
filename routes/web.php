<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/',                                     'HomeController@index')->name('home');
Route::get('/home',                                 'HomeController@index')->name('home');

/* Modulo Clientes */
Route::post('/clientes',                            'ClientesController@index')->name('clientes');
Route::post('/listClientes',                        'ClientesController@listClientes')->name('datatable.listclientes');
Route::post('/formClientes',                        'ClientesController@formClientes')->name('form.clientes');
Route::post('/saveformClientes',                    'ClientesController@saveFormCliente')->name('save.clientes');

/* Modulo Clientes */
Route::post('/proveedores',                         'ProveedoresController@index')->name('proveedores');
Route::post('/listProveedores',                     'ProveedoresController@listProveedores')->name('datatable.listproveedores');
Route::post('/formProveedores',                     'ProveedoresController@formProveedores')->name('form.proveedores');
Route::post('/saveformProveedores',                 'ProveedoresController@saveFormProveedor')->name('save.proveedores');

/* Modulo Productos */
Route::post('/productos',                           'ProductosController@index')->name('productos');
Route::post('/listProductos',                       'ProductosController@listProductos')->name('datatable.listproductos');
Route::post('/formProductos',                       'ProductosController@formProductos')->name('form.productos');
Route::post('/saveformProductos',                   'ProductosController@saveFormProductos')->name('save.productos');

/* Modulo Tipos Productos */
Route::post('/tipoproductos',                       'TipoProductosController@index')->name('tipoproductos');
Route::post('/listTipoProductos',                   'TipoProductosController@listTipoProductos')->name('datatable.listtipoproductos');
Route::post('/formTipoProductos',                   'TipoProductosController@formTipoProductos')->name('form.tipoproductos');
Route::post('/saveformTipoProductos',               'TipoProductosController@saveFormTipoProductos')->name('save.tipoproductos');

/* Modulo Productos Clientes */
Route::post('/productosclientes',                   'ProductoClienteController@index')->name('productosclientes');
Route::post('/listProductosClientes',               'ProductoClienteController@listProductosClientes')->name('datatable.listproductosclientes');
Route::post('/formProductosClientes',               'ProductoClienteController@formProductosClientes')->name('form.productosclientes');
Route::post('/saveformProductosClientes',           'ProductoClienteController@saveFormProductosClientes')->name('save.productosclientes');

/* Modulo Productos Proveedores */
Route::post('/productosproveedores',                'ProductoProveedorController@index')->name('productosclientes');
Route::post('/listProductosProveedores',            'ProductoProveedorController@listProductosProveedores')->name('datatable.listproductosproveedores');
Route::post('/formProductosProveedores',            'ProductoProveedorController@formProductosProveedores')->name('form.productosproveedores');
Route::post('/saveformProductosProveedores',        'ProductoProveedorController@saveFormProductosProveedores')->name('save.productosproveedores');

/* Modulo Graficos */
Route::post('/costosproveedor',                     'GraficosController@costosProveedor')->name('chart.costosproveedor');
Route::get('/getDataCostosProveedor',               'GraficosController@getDataCostosProveedor')->name('chart.datacostosproveedor');
Route::get('/getDataCostosProveedorTest',           'GraficosController@getDataCostosProveedorTest')->name('chart.datacostosproveedortest');

Route::post('/costossmsproveedor',                  'GraficosController@costosSMSProveedor')->name('chart.costossmsproveedor');
Route::get('/getDataCostosSMSProveedor',            'GraficosController@getDataCostosSMSProveedor')->name('chart.datacostossmsproveedor');


Route::post('/ventasclientes',                      'GraficosController@ventasCliente')->name('chart.ventasclientes');
Route::get('/getDataVentasClientes',                'GraficosController@getDataVentasCliente')->name('chart.dataventascliente');

Route::post('/rentabilidadproductocliente',         'GraficosController@rentabilidadClienteProducto')->name('chart.rentabilidadproductocliente');
Route::get('/getDataRentabilidadClienteProducto',   'GraficosController@getDataRentabilidadProductoCliente')->name('chart.datarenatbilidadproductocliente');

Route::post('/rentabilidadcliente',                 'GraficosController@rentabilidadCliente')->name('chart.rentabilidadcliente');
Route::get('/getDataRentabilidadCliente',           'GraficosController@getDataRentabilidadCliente')->name('chart.datarentabilidadcliente');

Route::post('/rentabilidadtelefoniacliente',        'GraficosController@rentabilidadTelefoniaCliente')->name('chart.rentabilidadtelefoniacliente');
Route::get('/getDataRentabilidadTelefoniaCliente',  'GraficosController@getDataRentabilidadTelefoniaCliente')->name('chart.datarentabilidadtelefoniacliente');

Route::post('/rentabilidadanual',                   'GraficosController@rentabilidadAnual')->name('chart.rentabilidadanual');
Route::get('/getDataRentabilidadAnual',             'GraficosController@getDataRentabilidadAnual')->name('chart.datarentabilidadanual');

Route::post('/costodiariotelefoniaproveedor',       'GraficosController@costodiarioTelefoniaProveedor')->name('chart.costodiariotelefoniaproveedor');
Route::get('/getDataCostoDiarioTelefoniaProveedor', 'GraficosController@getDataCostoDiarioTelefoniaProveedor')->name('chart.datacostodiariotelefoniaproveedor');

Route::post('/rentabilidadsmscliente',       'GraficosController@rentabilidadSMSCliente')->name('chart.rentabilidadsmscliente');
Route::get('/getDataRentabilidadSMSCliente', 'GraficosController@getDataRentabilidadSMSCliente')->name('chart.rentabilidadsmscliente');