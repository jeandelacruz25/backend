<?php

namespace Securitec;

use Illuminate\Database\Eloquent\Model;

class Meses extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'meses';
    protected $primaryKey   = 'mes';
    public    $timestamps   = false;

    protected $fillable = [
        'mes', 'DESC_LARGA', 'DESC_CORTA',
    ];
}
