<?php

namespace Securitec;

use Illuminate\Database\Eloquent\Model;

class Proveedores extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'proveedores';
    protected $primaryKey   = 'id_proveedor';
    public    $timestamps   = false;

    protected $fillable = [
        'id_proveedor', 'proveedor',
    ];
}
