<?php

namespace Securitec\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Securitec\Anios;
use Securitec\Clientes;
use Securitec\Meses;
use Securitec\Productos;
use Securitec\ProductosProveedor;
use Securitec\Proveedores;

class ProductoProveedorController extends SecuritecController
{
    public function index(Request $request)
    {
        $response = $request->user()->authorizeRoles(['admin']);
        $getOptions = $this->getOptions();
        if($response) return view('elements/productos/productosproveedores')->with(array(
            'titleModule'           => 'Lista de Productos Proveedores',
            'filterReport'          => true,
            'titleFilterReport'     => 'Productos Proveedores',
            'viewDateMonth'         => true,
            'viewDateYear'          => true,
            'viewClientes'          => false,
            'viewProveedores'       => true,
            'viewTipoProducto'      => false,
            'viewRangeDateAndMonth' => false,
            'optionsClientes'       => $getOptions['Clientes'],
            'optionsProveedores'    => $getOptions['Proveedores'],
            'optionsTipoProducto'   => '',
            'optionsMonth'          => $getOptions['Month'],
            'optionsYear'           => $getOptions['Year']
        ));
        return view('errors/autorizacion');
    }

    public function listProductosProveedores(Request $request)
    {
        if ($request->isMethod('post')) {
            $query_producto_proveedor_list  = $this->productos_proveedores_list_query($request->selectedProveedor, $request->selectedMonth, $request->selectedYear);
            $builderview                    = $this->builderview($query_producto_proveedor_list);
            $outgoingcollection             = $this->outgoingcollection($builderview);
            $list_productos_clientes        = $this->FormatDatatable($outgoingcollection);
            return $list_productos_clientes;
        }
    }

    protected function productos_proveedores_list_query($idProveedor, $dateMonth, $dateYear)
    {
        $productos_proveedores_list_query = ProductosProveedor::Select()
            ->with('proveedor')
            ->with('producto')
            ->where('id_proveedor', $idProveedor)
            ->where('anio', $dateYear)
            ->where('mes', $dateMonth)
            ->get()
            ->toArray();

        return $productos_proveedores_list_query;
    }

    protected function builderview($producto_proveedor_list_query,$type='')
    {
        $posicion = 0;
        $idList = 0;
        foreach ($producto_proveedor_list_query as $query) {

            $idList ++;
            $builderview[$posicion]['id']                       = $idList;
            $builderview[$posicion]['id_producto_proveedor']    = $query['id'];
            $builderview[$posicion]['proveedor']                = ucwords(Str::lower($query['proveedor']['proveedor']));
            $builderview[$posicion]['producto']                 = ucwords(Str::lower($query['producto']['producto']));
            $builderview[$posicion]['cantidad']                 = $query['cantidad'];
            $builderview[$posicion]['precio_costo']             = number_format($query['precio_costo'], 4, '.', ',');
            $posicion ++;
        }
        if(!isset($builderview)){
            $builderview = [];
        }
        return $builderview;
    }

    protected function outgoingcollection($builderview)
    {
        $outgoingcollection = new \Illuminate\Support\Collection;
        foreach ($builderview as $view) {
            $outgoingcollection->push([
                'id'                => $view['id'],
                'proveedor'         => $view['proveedor'],
                'producto'          => $view['producto'],
                'cantidad'          => $view['cantidad'],
                'precio_costo'      => $view['precio_costo'],
                'action'            => '<span data-toggle="tooltip" data-placement="left" title="Editar Producto Proveedor"><a class="btn btn-warning btn-xs" onclick="responseModal('."'div.dialogSecuritec','formProductosProveedores','".$view['id_producto_proveedor']."'".')" data-toggle="modal" data-target="#modalSecuritec"><i class="fa fa-edit" aria-hidden="true"></i></a></span>',
            ]);
        }
        return $outgoingcollection;
    }

    public function formProductosProveedores(Request $request)
    {
        $response = $request->user()->authorizeRoles(['admin']);
        if($response) {
            if($request->valueID == null){
                $idProductoProveedor = ProductosProveedor::max('id');
                $getOptions = $this->getOptions();
                return view('elements/productos/form/formProveedorProductos')->with(array(
                    'idProductoProveedor'   => ($idProductoProveedor ? $idProductoProveedor + 1:1),
                    'idEstado'              => '',
                    'optionsProveedores'    => $getOptions['Proveedores'],
                    'optionsProductos'      => $getOptions['Productos'],
                    'optionsMonth'          => $getOptions['Month'],
                    'optionsYear'           => $getOptions['Year'],
                    'selectedProveedores'   => '',
                    'selectedProductos'     => '',
                    'selectedMonth'         => '',
                    'selectedYear'          => '',
                    'numCantidad'           => '',
                    'numPrecioCosto'        => '',
                    'updateForm'            => false
                ));
            }else{
                $getProductoProveedor = $this->getProductoProveedor($request->valueID);
                $getOptions = $this->getOptions();
                return view('elements/productos/form/formProveedorProductos')->with(array(
                    'idProductoProveedor'   => $request->valueID,
                    'idEstado'              => $getProductoProveedor[0]['id_estado'],
                    'optionsProveedores'    => $getOptions['Proveedores'],
                    'optionsProductos'      => $getOptions['Productos'],
                    'optionsMonth'          => $getOptions['Month'],
                    'optionsYear'           => $getOptions['Year'],
                    'selectedProveedores'   => $getProductoProveedor[0]['id_proveedor'],
                    'selectedProductos'     => $getProductoProveedor[0]['id_producto'],
                    'selectedMonth'         => $getProductoProveedor[0]['mes'],
                    'selectedYear'          => $getProductoProveedor[0]['anio'],
                    'numCantidad'           => $getProductoProveedor[0]['cantidad'],
                    'numPrecioCosto'        => $getProductoProveedor[0]['precio_costo'],
                    'updateForm'            => true
                ));
            }

        }
        return view('errors/autorizacion');
    }

    public function getOptions()
    {
        $clientes = Clientes::Select()
            ->get()
            ->toArray();

        $proveedores = Proveedores::Select()
            ->get()
            ->toArray();

        $productos = Productos::Select()
            ->get()
            ->toArray();

        $month = Meses::Select()
            ->get()
            ->toArray();

        $year = Anios::Select()
            ->get()
            ->toArray();

        $options['Clientes'] = $clientes;
        $options['Proveedores'] = $proveedores;
        $options['Productos'] = $productos;
        $options['Month'] = $month;
        $options['Year'] = $year;

        return $options;
    }

    public function getProductoProveedor($idProductoProveedor)
    {
        $productoCliente = ProductosProveedor::Select()
            ->where('id', $idProductoProveedor)
            ->get()
            ->toArray();
        return $productoCliente;
    }

    public function saveFormProductosProveedores(Request $request)
    {
        if ($request->isMethod('post')) {
            try{
                DB::beginTransaction();
                ProductosProveedor::updateOrCreate([
                    'id'            => $request->productoProveedorID
                ], [
                    'id_proveedor'  => $request->selectedProveedor,
                    'id_producto'   => $request->selectedProducto,
                    'anio'          => $request->selectedAnio,
                    'mes'           => $request->selectedMonth,
                    'cantidad'      => $request->numCantidad,
                    'precio_costo'  => $request->numPrecioCosto,
                    'id_estado'     => $request->estadoID
                ]);

                /*$request->productoProveedorID ? DB::select('CALL sp_inserta_movestadistica_proveedor ("'.$request->selectedAnio.'","'.$request->selectedMonth.'","'.$request->selectedProveedor.'","'.$request->selectedProducto.'","'.$request->numCantidad.'","'.$request->numPrecioCosto.'")') : '';*/

                DB::commit();

                $action = ($request->productoProveedorID ? 'Actualizo' : 'Creo');
                return ['message' => 'Success', 'action' => $action];
            } catch(\Exception $e){
                DB::rollback();
                return ['message' => 'Error'];
            }
        }
        return ['message' => 'Error'];
    }
}
