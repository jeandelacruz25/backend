<?php

namespace Securitec\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class SecuritecController extends Controller
{
    /**
     * [FormatDatatable Función que retorna en formato Datatable]
     * @param [Collection] $collection [Collection con los datos a mostrar en los reportes]
     */
    protected function FormatDatatable($collection){
        return DataTables::of($collection)
            ->make(true);
    }

    protected function arrayColors(){
        return [
            '#3498db','#2ecc71','#9343ED','#9b59b6','#34495e','#f1c40f','#e67e22','#e74c3c','#fcdd00','#95a5a6'
        ];
    }

    protected function chartBarMultiTable($labelChart, $dataChart){
        $countData = 0;
        $tableData = "<div class='scrollTable'><table id='sum_table' class='table table-bordered table-responsive text-center'>";
            $tableData .= "<tr class='text-bold'><td>#</td>";
            foreach ($labelChart as $key => $data){
                $tableData .= "<td class='text-bold'>".$data."</td>";
                $countData ++;
            }

            $tableData .= "</tr>";
            foreach ($dataChart as $key => $data){
                $tableData .= "<tr>";
                    $tableData .= "<td>".$key."</td>";
                    for ($i = 0; $i < $countData; $i++){
                        $labelData = isset($data[$i]) ? $data[$i] : 0;
                        $tableData .= "<td class='rowDataSd'>".$labelData."</td>";
                    }
                $tableData .= "</tr>";
            }

            $tableData .= "<tr class='totalColumn bg-primary'>";
            $tableData .= "<td class='text-bold'>Total:</td>";
                for ($i = 0; $i < $countData; $i++){
                    $tableData .= "<td class='totalCol'></td>";
                }
            $tableData .= "</tr>";

        $tableData .= "</table></div>";
        return $tableData;
    }

    protected function chartBarTable($labelChart, $dataChart){
        $tableData = "<div class='scrollTable'><table id='sum_table' class='table table-bordered table-responsive text-center'>";
        $tableData .= "<tr class='text-bold'><td>#</td><td class='text-bold'>Precio</td>";

        $tableData .= "</tr>";

        foreach ($dataChart as $key => $data){
            $tableData .= "<tr>";
            $tableData .= "<td>".$labelChart[$key]."</td>";
            $labelData = isset($data) ? $data : 0;
            $tableData .= "<td class='rowDataSd'>".$labelData."</td>";
            $tableData .= "</tr>";
        }

        $tableData .= "<tr class='totalColumn bg-primary'>";
        $tableData .= "<td class='text-bold'>Total:</td>";
        $tableData .= "<td class='totalCol'></td>";
        $tableData .= "</tr>";

        $tableData .= "</table></div>";
        return $tableData;
    }
}
