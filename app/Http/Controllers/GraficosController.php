<?php

namespace Securitec\Http\Controllers;

use Carbon\Carbon;
use ConsoleTVs\Charts\Facades\Charts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Securitec\Anios;
use Securitec\Clientes;
use Securitec\Meses;
use Securitec\Proveedores;
use Securitec\TipoProductos;

class GraficosController extends SecuritecController
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function costosProveedor()
    {
        $getOptions = $this->getOptions();
        return view('elements/graficos/costos_proveedor')->with(array(
            'titleModule'           => 'Grafico Costos Telefonia por Proveedor',
            'filterReport'          => true,
            'titleFilterReport'     => 'Costos Telefonia por Proveedor',
            'viewDateMonth'         => false,
            'viewDateYear'          => false,
            'viewClientes'          => false,
            'viewProveedores'       => false,
            'viewTipoProducto'      => false,
            'viewRangeDateAndMonth' => true,
            'optionsClientes'       => '',
            'optionsProveedores'    => '',
            'optionsTipoProducto'   => '',
            'optionsMonth'          => $getOptions['Month'],
            'optionsYear'           => $getOptions['Year']
        ));
    }

    public function getDataCostosProveedor(Request $request)
    {

        $filterFechas = $request->selectFilter;

        if($filterFechas === 'mesanio'){
            $dataCostosProveedor =
                DB::select('
                    SELECT m.id_proveedor,
                        pr.proveedor,
                        m.id_producto,
                        p.producto, 
                        ROUND(sum(m.cantidad)/60, 2) total_min,
                        ROUND(sum(m.precio_costo), 2) total_costo
                    FROM estadistica m, productos p, proveedores pr
                    WHERE m.id_producto = p.id_producto AND
                        m.id_proveedor = pr.id_proveedor AND
                        m.anio = '.$request->selectedYear.' AND
                        m.mes = '.$request->selectedMonth.' AND
                         m.id_producto IN (2,3,4)
                    GROUP BY m.id_proveedor, pr.proveedor, m.id_producto, p.producto
                    ORDER BY m.id_proveedor, m.id_producto;
               ');
        }else{
            $dateRange = explode(' - ', $request->selectRange);
            $dataCostosProveedor =
                DB::select('
                    SELECT m.id_proveedor,
                           pr.proveedor,
                           m.id_producto,
                           p.producto, 
                           ROUND(SUM(m.cantidad)/60, 2) total_min,
                           ROUND(SUM(m.cantidad*m.precio_costo), 2) total_costo
                    FROM movestadistica m, productos p, proveedores pr
                    WHERE m.id_producto = p.id_producto AND
                          m.id_proveedor = pr.id_proveedor AND
                          m.fecha >= "'.$dateRange[0].'" AND
                          m.fecha <= "'.$dateRange[1].'" AND
                           m.id_producto IN (2,3,4)
                    GROUP BY m.id_proveedor, pr.proveedor, m.id_producto, p.producto
                    ORDER BY m.id_proveedor, m.id_producto;
                ');
        }

        if(!empty($dataCostosProveedor)){
            $dataLabel = collect($dataCostosProveedor)->map(function ($item, $key) {
                return $item->proveedor;
            })->unique()->values()->toArray();

            $labeldataSets = collect($dataCostosProveedor)->values()->mapToGroups(function ($item, $key) {
                return [$item->producto => $item->total_costo];
            })->toArray();

            $dataTable = $this->chartBarMultiTable($dataLabel, $labeldataSets);

            $chart = Charts::multi('bar', 'chartjs')
                ->title("Costos Telefonia por Proveedor")
                ->dimensions(0, 400)
                ->colors($this->arrayColors())
                ->labels($dataLabel);

            foreach ($labeldataSets as $key => $value){ $chart->dataset($key, $value); }

            return response()->json([
                'chartHTML' => $chart->html(),
                'chartScript' => $chart->script(),
                'chartTable' => $dataTable
            ]);
        }else{
            $chart = Charts::multi('bar', 'chartjs')
                ->title("Costos Telefonia por Proveedor")
                ->dimensions(0, 400)
                ->colors($this->arrayColors())
                ->labels([]);

            $dataTable = [];

            return response()->json([
                'chartHTML' => '',
                'chartScript' => '',
                'chartTable' => ''
            ]);
        }
    }

    public function costosSMSProveedor()
    {
        $getOptions = $this->getOptions();
        return view('elements/graficos/costos_sms_proveedor')->with(array(
            'titleModule'           => 'Grafico Costos SMS por Proveedor',
            'filterReport'          => true,
            'titleFilterReport'     => 'Costos SMS por Proveedor',
            'viewDateMonth'         => false,
            'viewDateYear'          => false,
            'viewClientes'          => false,
            'viewProveedores'       => false,
            'viewTipoProducto'      => false,
            'viewRangeDateAndMonth' => true,
            'optionsClientes'       => '',
            'optionsProveedores'    => '',
            'optionsTipoProducto'   => '',
            'optionsMonth'          => $getOptions['Month'],
            'optionsYear'           => $getOptions['Year']
        ));
    }

    public function getDataCostosSMSProveedor(Request $request)
    {

        $filterFechas = $request->selectFilter;

        if($filterFechas === 'mesanio'){
            $dataCostosProveedor =
                DB::select('
                    SELECT m.id_proveedor,
                        pr.proveedor,
                        m.id_producto,
                        p.producto, 
                        ROUND(sum(m.cantidad)/60, 2) total_min,
                        ROUND(sum(m.precio_costo), 2) total_costo
                    FROM estadistica m, productos p, proveedores pr
                    WHERE m.id_producto = p.id_producto AND
                        m.id_proveedor = pr.id_proveedor AND
                        m.anio = '.$request->selectedYear.' AND
                        m.mes = '.$request->selectedMonth.' AND
                        m.id_producto = 28
                    GROUP BY m.id_proveedor, pr.proveedor, m.id_producto, p.producto
                    ORDER BY m.id_proveedor, m.id_producto;
               ');
        }else{
            $dateRange = explode(' - ', $request->selectRange);
            $dataCostosProveedor =
                DB::select('
                    SELECT m.id_proveedor,
                           pr.proveedor,
                           m.id_producto,
                           p.producto, 
                           ROUND(SUM(m.cantidad)/60, 2) total_min,
                           ROUND(SUM(m.cantidad*m.precio_costo), 2) total_costo
                    FROM movestadistica m, productos p, proveedores pr
                    WHERE m.id_producto = p.id_producto AND
                          m.id_proveedor = pr.id_proveedor AND
                          m.fecha >= "'.$dateRange[0].'" AND
                          m.fecha <= "'.$dateRange[1].'" AND
                          m.id_producto = 28
                    GROUP BY m.id_proveedor, pr.proveedor, m.id_producto, p.producto
                    ORDER BY m.id_proveedor, m.id_producto;
                ');
        }

        if(!empty($dataCostosProveedor)){
            $dataLabel = collect($dataCostosProveedor)->map(function ($item, $key) {
                return $item->proveedor;
            })->unique()->values()->toArray();

            $labeldataSets = collect($dataCostosProveedor)->values()->mapToGroups(function ($item, $key) {
                return [$item->producto => $item->total_costo];
            })->toArray();

            $dataTable = $this->chartBarMultiTable($dataLabel, $labeldataSets);

            $chart = Charts::multi('bar', 'chartjs')
                ->title("Costos SMS por Proveedor")
                ->dimensions(0, 400)
                ->colors($this->arrayColors())
                ->labels($dataLabel);

            foreach ($labeldataSets as $key => $value){ $chart->dataset($key, $value); }

            return response()->json([
                'chartHTML' => $chart->html(),
                'chartScript' => $chart->script(),
                'chartTable' => $dataTable
            ]);
        }else{
            $chart = Charts::multi('bar', 'chartjs')
                ->title("Costos SMS por Proveedor")
                ->dimensions(0, 400)
                ->colors($this->arrayColors())
                ->labels([]);

            $dataTable = [];

            return response()->json([
                'chartHTML' => '',
                'chartScript' => '',
                'chartTable' => ''
            ]);
        }
    }

    public function getDataCostosProveedorTest(Request $request)
    {
        $dataCostosProveedor =
        DB::select('
            SELECT m.id_proveedor,
               pr.proveedor,
               m.id_producto,
               p.producto, 
               ROUND(SUM(m.cantidad)/60, 2) total_min,
               ROUND(SUM(m.cantidad*m.precio_costo), 2) total_costo
              FROM movestadistica m, productos p, proveedores pr
              WHERE m.id_producto = p.id_producto AND
                m.id_proveedor = pr.id_proveedor AND
                m.anio = '.$request->selectedYear.' AND
                m.mes = '.$request->selectedMonth.' AND
                m.cantidad > 0 AND
                m.id_tipoproducto=2
            GROUP BY m.id_proveedor, pr.proveedor, m.id_producto, p.producto
            ORDER BY m.id_proveedor, m.id_producto;
       ');

        $dataCostosProveedor1 =
        DB::select('
            SELECT m.id_proveedor,
               pr.proveedor,
               m.id_producto,
               p.producto, 
               ROUND(SUM(m.cantidad)/60, 2) total_min,
               ROUND(SUM(m.cantidad*m.precio_costo), 2) total_costo
              FROM movestadistica m, productos p, proveedores pr
              WHERE m.id_producto = p.id_producto AND
                m.id_proveedor = pr.id_proveedor AND
                m.anio = '.$request->selectedYear.' AND
                m.mes = '.$request->selectedMonth.' AND
                m.id_tipoproducto=2
            GROUP BY m.id_proveedor, pr.proveedor, m.id_producto, p.producto
            ORDER BY m.id_proveedor, m.id_producto;
       ');

        $dataLabel = collect($dataCostosProveedor)->map(function ($item, $key) {
            return $item->proveedor;
        })->unique()->values()->toArray();

        $dataProductos = collect($dataCostosProveedor)->map(function ($item, $key) {
            return $item->producto;
        })->unique()->values()->toArray();

        var_dump($dataLabel );
        var_dump($dataProductos);

        $dataLabel1 = collect($dataCostosProveedor1)->map(function ($item, $key) {
            return $item->proveedor;
        })->unique()->values()->toArray();

        $dataProductos1 = collect($dataCostosProveedor1)->map(function ($item, $key) {
            return $item->producto;
        })->unique()->values()->toArray();

        var_dump($dataLabel1 );
        var_dump($dataProductos1);

        //var_dump($dataLabel);

        $labeldataSets = collect($dataCostosProveedor)->values()->mapToGroups(function ($item, $key) {
            return [$item->producto => $item->total_costo];
        })->toArray();

        $collection = collect(['name', 'age']);
        //var_dump($collection);

        $dataTable = $this->chartBarMultiTable($dataLabel, $labeldataSets);

        $chart = Charts::multi('bar', 'chartjs')
            ->title("Costos por Proveedor")
            ->dimensions(0, 400)
            ->colors($this->arrayColors())
            ->labels($dataLabel);

        foreach ($labeldataSets as $key => $value){ $chart->dataset($key, $value); }

        return response()->json([
            'chartHTML' => '',
            'chartScript' => '',
            'chartTable' => ''
        ]);
    }

    public function ventasCliente()
    {
    $getOptions = $this->getOptions();
    return view('elements/graficos/ventas_cliente')->with(array(
        'titleModule'           => 'Grafico Ventas Clientes',
        'filterReport'          => true,
        'titleFilterReport'     => 'Ventas Clientes',
        'viewDateMonth'         => true,
        'viewDateYear'          => true,
        'viewClientes'          => true,
        'viewProveedores'       => false,
        'viewTipoProducto'      => false,
        'viewRangeDateAndMonth' => false,
        'optionsClientes'       => $getOptions['Clientes'],
        'optionsProveedores'    => '',
        'optionsTipoProducto'   => '',
        'optionsMonth'          => $getOptions['Month'],
        'optionsYear'           => $getOptions['Year']
    ));
}

    public function getDataVentasCliente(Request $request)
    {
        $dataVentasCliente =
            DB::select('
                SELECT m.id_cliente, c.cliente, p.id_producto, p.producto,
                       round(if(p.id_producto=1, sum(m.cantidad), round(sum(m.cantidad)/60, 2)), 2) cantidad,
                       round(sum(m.precio_venta), 2) total_venta
                  FROM estadistica m , productos p, clientes c
                  WHERE m.id_producto = p.id_producto AND
                    m.id_cliente = c.id_cliente AND
                    m.anio = '.$request->selectedYear.' AND
                    m.mes = '.$request->selectedMonth.' AND
                    m.id_cliente = '.$request->selectedClientes.'
                GROUP BY m.id_cliente, c.cliente, p.id_producto, p.producto
                ORDER BY m.id_cliente, p.id_producto;
           ');

        $dataLabel = collect($dataVentasCliente)->map(function ($item, $key) {
            return $item->producto;
        })->unique()->values()->toArray();

        $labeldataSets = collect($dataVentasCliente)->values()->map(function ($item, $key) {
            return $item->total_venta;
        })->toArray();

        $dataTable = $this->chartBarTable($dataLabel, $labeldataSets);

        $chart = Charts::create('bar', 'chartjs')
            ->title("Ventas por Clientes")
            ->dimensions(0, 400)
            ->elementLabel('Total de Venta')
            ->colors(['#3498db'])
            ->labels($dataLabel)
            ->values($labeldataSets);

        return response()->json([
            'chartHTML' => $chart->html(),
            'chartScript' => $chart->script(),
            'chartTable' => $dataTable
        ]);
    }

    public function rentabilidadClienteProducto()
    {
        $getOptions = $this->getOptions();
        return view('elements/graficos/rentabilidad_producto_cliente')->with(array(
            'titleModule'           => 'Grafico Rentabilidad Tipo Producto y Cliente',
            'filterReport'          => true,
            'titleFilterReport'     => 'Rentabilidad Tipo Producto y Cliente',
            'viewDateMonth'         => true,
            'viewDateYear'          => true,
            'viewClientes'          => true,
            'viewProveedores'       => false,
            'viewTipoProducto'      => false,
            'viewRangeDateAndMonth' => false,
            'optionsClientes'       => $getOptions['Clientes'],
            'optionsProveedores'    => '',
            'optionsTipoProducto'   => '',
            'optionsMonth'          => $getOptions['Month'],
            'optionsYear'           => $getOptions['Year']
        ));
    }

    public function getDataRentabilidadProductoCliente(Request $request)
    {   

        if($request->selectedClientes){
            
            $dataRentabilidadProductoCliente =
            DB::select('
                SELECT m.id_cliente, c.cliente, m.id_tipoproducto, tp.tipo_producto,
                   ROUND(SUM(m.precio_costo), 2) compra,
                   ROUND(SUM(m.precio_venta), 2) venta,
                   ROUND(SUM(m.precio_venta - m.precio_costo), 2) rentabilidad
                FROM estadistica m , tipoproductos tp, clientes c
                WHERE m.id_cliente = c.id_cliente AND
                    m.id_tipoproducto = tp.id_tipoproducto AND
                    m.anio = '.$request->selectedYear.' AND
                    m.mes = '.$request->selectedMonth.' AND
                    m.id_cliente = '.$request->selectedClientes.'
                GROUP BY m.id_cliente, c.cliente, m.id_tipoproducto, tp.tipo_producto
                ORDER BY m.id_cliente, m.id_tipoproducto;
            ');
        }else{
            

            $dataRentabilidadProductoCliente =
            DB::select('
                SELECT tp.id_tipoproducto, tp.tipo_producto,
                       IFNULL(ROUND(SUM(m.precio_venta - m.precio_costo), 2), 0) rentabilidad
                FROM tipoproductos tp     
                LEFT JOIN estadistica m ON tp.id_tipoproducto  = m.id_tipoproducto AND
                m.anio = '.$request->selectedYear.' AND
                m.mes = '.$request->selectedMonth.' 
                GROUP BY tp.id_tipoproducto, tp.tipo_producto ORDER BY rentabilidad DESC;
            ');

            //dd($dataRentabilidadProductoCliente);
            //die();
        }



        

        $dataLabel = collect($dataRentabilidadProductoCliente)->map(function ($item, $key) {
            return $item->tipo_producto;
        })->unique()->values()->toArray();

        

        $labeldataSets = collect($dataRentabilidadProductoCliente)->values()->map(function ($item, $key) {
            return $item->rentabilidad;
        })->toArray();

        //dd($labeldataSets);
        $dataTable = $this->chartBarTable($dataLabel, $labeldataSets);

        $chart = Charts::create('bar', 'chartjs')
            ->title("Rentabilidad por Tipo de Producto y Cliente")
            ->dimensions(0, 400)
            ->colors(['#3498db'])
            ->elementLabel('Total Rentabilidad')
            ->labels($dataLabel)
            ->values($labeldataSets);

        return response()->json([
            'chartHTML' => $chart->html(),
            'chartScript' => $chart->script(),
            'chartTable' => $dataTable
        ]);
    }

    public function rentabilidadCliente()
    {
        $getOptions = $this->getOptions();
        return view('elements/graficos/rentabilidad_cliente')->with(array(
            'titleModule'           => 'Grafico Rentabilidad Total Cliente',
            'filterReport'          => true,
            'titleFilterReport'     => 'Rentabilidad Total Cliente',
            'viewDateMonth'         => true,
            'viewDateYear'          => true,
            'viewClientes'          => false,
            'viewProveedores'       => false,
            'viewTipoProducto'      => false,
            'viewRangeDateAndMonth' => false,
            'optionsClientes'       => '',
            'optionsProveedores'    => '',
            'optionsTipoProducto'   => '',
            'optionsMonth'          => $getOptions['Month'],
            'optionsYear'           => $getOptions['Year']
        ));
    }

    public function getDataRentabilidadCliente(Request $request)
    {
        $dataRentabilidadCliente =
            DB::select('
                SELECT m.id_cliente, c.cliente,
                       ROUND(SUM(m.precio_venta - m.precio_costo), 2) rentabilidad_total
                  FROM estadistica m , clientes c
                  WHERE m.id_cliente = c.id_cliente AND
                    m.anio = '.$request->selectedYear.' AND
                    m.mes = '.$request->selectedMonth.'
                GROUP BY m.id_cliente, c.cliente
                ORDER BY m.id_cliente;
            ');

        $dataLabel = collect($dataRentabilidadCliente)->map(function ($item, $key) {
            return $item->cliente;
        })->unique()->values()->toArray();

        $labeldataSets = collect($dataRentabilidadCliente)->map(function ($item, $key) {
            return $item->rentabilidad_total;
        })->values()->toArray();

        $dataTable = $this->chartBarTable($dataLabel, $labeldataSets);

        $chart = Charts::create('bar', 'chartjs')
            ->title("Rentabilidad Total por Cliente")
            ->dimensions(0, 400)
            ->colors(['#3498db'])
            ->elementLabel('Total Rentabilidad')
            ->labels($dataLabel)
            ->values($labeldataSets);

        return response()->json([
            'chartHTML' => $chart->html(),
            'chartScript' => $chart->script(),
            'chartTable' => $dataTable
        ]);
    }

    public function rentabilidadTelefoniaCliente()
    {
        $getOptions = $this->getOptions();
        return view('elements/graficos/rentabilidad_telefonia_cliente')->with(array(
            'titleModule'           => 'Grafico Rentabilidad Diaria Telefonía Cliente',
            'filterReport'          => true,
            'titleFilterReport'     => 'Rentabilidad Telefonía Cliente',
            'viewDateMonth'         => true,
            'viewDateYear'          => true,
            'viewClientes'          => true,
            'viewProveedores'       => false,
            'viewTipoProducto'      => false,
            'viewRangeDateAndMonth' => false,
            'optionsClientes'       => $getOptions['Clientes'],
            'optionsProveedores'    => '',
            'optionsTipoProducto'   => '',
            'optionsMonth'          => $getOptions['Month'],
            'optionsYear'           => $getOptions['Year']
        ));
    }

    public function getDataRentabilidadTelefoniaCliente(Request $request)
    {
        if($request->selectedClientes){
            $dataRentabilidadTelefoniaCliente =
                DB::select('
                SELECT m.fecha, m.id_tipoproducto, tp.tipo_producto,
                       round(sum(m.cantidad*m.precio_costo), 2) compra,
                       round(sum(m.cantidad*m.precio_venta), 2) venta,
                       round(sum(m.cantidad*(m.precio_venta - m.precio_costo)), 2) rentabilidad
                  FROM movestadistica m , tipoproductos tp, clientes c
                  WHERE m.id_cliente = c.id_cliente AND
                        m.id_tipoproducto = tp.id_tipoproducto AND
                        m.id_tipoproducto = 2 AND
                        m.anio = '.$request->selectedYear.' AND
                        m.mes = '.$request->selectedMonth.' AND
                        m.id_cliente = '.$request->selectedClientes.'
                GROUP BY m.fecha, m.id_tipoproducto, tp.tipo_producto
                ORDER BY m.fecha, m.id_tipoproducto;
            ');
        }else{
            $dataRentabilidadTelefoniaCliente =
                DB::select('
                SELECT m.fecha, m.id_tipoproducto, tp.tipo_producto,
                       round(sum(m.cantidad*m.precio_costo), 2) compra,
                       round(sum(m.cantidad*m.precio_venta), 2) venta,
                       round(sum(m.cantidad*(m.precio_venta - m.precio_costo)), 2) rentabilidad
                  FROM movestadistica m , tipoproductos tp, clientes c
                  WHERE m.id_cliente = c.id_cliente AND
                        m.id_tipoproducto = tp.id_tipoproducto AND
                        m.id_tipoproducto = 2 AND
                        m.anio = '.$request->selectedYear.' AND
                        m.mes = '.$request->selectedMonth.'
                GROUP BY m.fecha, m.id_tipoproducto, tp.tipo_producto
                ORDER BY m.fecha, m.id_tipoproducto;
            ');
        }

        $dataLabel = collect($dataRentabilidadTelefoniaCliente)->map(function ($item, $key) {
            return Carbon::parse($item->fecha)->format('j');
        })->unique()->values()->toArray();

        $labeldataSets = collect($dataRentabilidadTelefoniaCliente)->map(function ($item, $key) {
            return $item->rentabilidad;
        })->values()->toArray();

        $dataTable = $this->chartBarTable($dataLabel, $labeldataSets);

        $chart = Charts::create('area', 'chartjs')
            ->title("Rentabilidad Diaria de Telefonía por Cliente")
            ->dimensions(0, 400)
            ->colors($this->arrayColors())
            ->elementLabel('Total Rentabilidad')
            ->labels($dataLabel)
            ->values($labeldataSets);

        return response()->json([
            'chartHTML' => $chart->html(),
            'chartScript' => $chart->script(),
            'chartTable' => $dataTable
        ]);
    }

    public function costodiarioTelefoniaProveedor()
    {
        $getOptions = $this->getOptions();
        return view('elements/graficos/costo_telefonia_proveedor')->with(array(
            'titleModule'           => 'Grafico Costo Diario Telefonía Proveedor',
            'filterReport'          => true,
            'titleFilterReport'     => 'Costo Telefonía Proveedor',
            'viewDateMonth'         => true,
            'viewDateYear'          => true,
            'viewClientes'          => true,
            'viewProveedores'       => false,
            'viewTipoProducto'      => true,
            'viewRangeDateAndMonth' => false,
            'optionsClientes'       => $getOptions['Clientes'],
            'optionsProveedores'    => '',
            'optionsTipoProducto'   => $getOptions['TipoProducto'],
            'optionsMonth'          => $getOptions['Month'],
            'optionsYear'           => $getOptions['Year']
        ));
    }

    public function getDataCostoDiarioTelefoniaProveedor(Request $request)
    {

        if($request->selectedClientes){
            $dataCostoTelefoniaProveeedor =
                DB::select('
                SELECT m.fecha, p.proveedor,
                       ROUND(SUM(m.cantidad*m.precio_costo), 2) compra
                  FROM movestadistica m , tipoproductos tp, proveedores p
                  WHERE m.id_proveedor = p.id_proveedor AND
                        m.id_tipoproducto = tp.id_tipoproducto AND
                        m.anio = '.$request->selectedYear.' AND
                        m.mes = '.$request->selectedMonth.' AND
                        m.id_tipoproducto = '.$request->selectedTipoProducto.' AND
                        m.id_cliente = '.$request->selectedClientes.' 
                GROUP BY m.fecha, p.proveedor
                ORDER BY m.fecha, p.proveedor;
            ');
        }else{
            $dataCostoTelefoniaProveeedor =
                DB::select('
                SELECT m.fecha, p.proveedor,
                       ROUND(SUM(m.cantidad*m.precio_costo), 2) compra
                  FROM movestadistica m , tipoproductos tp, proveedores p
                  WHERE m.id_proveedor = p.id_proveedor AND
                        m.id_tipoproducto = tp.id_tipoproducto AND
                        m.anio = '.$request->selectedYear.' AND
                        m.mes = '.$request->selectedMonth.' AND
                        m.id_tipoproducto = '.$request->selectedTipoProducto.'
                GROUP BY m.fecha, p.proveedor
                ORDER BY m.fecha, p.proveedor;
            ');
        }

        $dataLabel = collect($dataCostoTelefoniaProveeedor)->values()->map(function ($item, $key) {
            return Carbon::parse($item->fecha)->format('d');
        })->unique()->values()->toArray();

        $labeldataSets = collect($dataCostoTelefoniaProveeedor)->values()->mapToGroups(function ($item, $key) {
            return [$item->proveedor => $item->compra];
        })->toArray();

        $dataTable = $this->chartBarMultiTable($dataLabel, $labeldataSets);

        $chart = Charts::multi('line', 'chartjs')
            ->title("Costo Diaria de Telefonía por Proverdor")
            ->dimensions(0, 400)
            ->colors($this->arrayColors())
            ->elementLabel('Total Costo')
            ->labels($dataLabel);

        foreach ($labeldataSets as $key => $value){ $chart->dataset($key, $value); }

        return response()->json([
            'chartHTML' => $chart->html(),
            'chartScript' => $chart->script(),
            'chartTable' => $dataTable
        ]);
    }

    public function rentabilidadAnual()
    {
        $getOptions = $this->getOptions();
        return view('elements/graficos/rentabilidad_anual')->with(array(
            'titleModule'           => 'Grafico Rentabilidad Anual',
            'filterReport'          => true,
            'titleFilterReport'     => 'Rentabilidad Anual',
            'viewDateMonth'         => false,
            'viewDateYear'          => true,
            'viewClientes'          => false,
            'viewProveedores'       => false,
            'viewTipoProducto'      => false,
            'viewRangeDateAndMonth' => false,
            'optionsClientes'       => '',
            'optionsProveedores'    => '',
            'optionsTipoProducto'   => '',
            'optionsMonth'          => '',
            'optionsYear'           => $getOptions['Year']
        ));
    }

    public function getDataRentabilidadAnual(Request $request)
    {
        $dataRentabilidadAnual =
            DB::select('
                SELECT m.mes, mm.desc_corta,
                     round(sum(m.precio_venta - m.precio_costo), 2) rentabilidad_total
                FROM estadistica m, meses mm
                WHERE m.mes   = mm.mes AND
                      m.anio = '.$request->selectedYear.'
                GROUP BY m.mes, mm.desc_corta
                ORDER BY m.mes;
            ');

        $dataLabel = collect($dataRentabilidadAnual)->map(function ($item, $key) {
            return $item->desc_corta;
        })->unique()->values()->toArray();

        $labeldataSets = collect($dataRentabilidadAnual)->map(function ($item, $key) {
            return $item->rentabilidad_total;
        })->values()->toArray();

        $dataTable = $this->chartBarTable($dataLabel, $labeldataSets);

        $chart = Charts::create('bar', 'chartjs')
            ->title("Rentabilidad Total por Meses")
            ->dimensions(0, 400)
            ->colors(['#3498db'])
            ->elementLabel('Total Rentabilidad')
            ->labels($dataLabel)
            ->values($labeldataSets);

        return response()->json([
            'chartHTML' => $chart->html(),
            'chartScript' => $chart->script(),
            'chartTable' => $dataTable
        ]);
    }

    public function rentabilidadSMSCliente()
    {
        $getOptions = $this->getOptions();
        return view('elements/graficos/rentabilidad_sms_cliente')->with(array(
            'titleModule'           => 'Grafico Rentabilidad SMS Cliente',
            'filterReport'          => true,
            'titleFilterReport'     => 'Rentabilidad SMS y Cliente',
            'viewDateMonth'         => true,
            'viewDateYear'          => true,
            'viewClientes'          => true,
            'viewProveedores'       => false,
            'viewTipoProducto'      => false,
            'viewRangeDateAndMonth' => false,
            'optionsClientes'       => $getOptions['Clientes'],
            'optionsProveedores'    => '',
            'optionsTipoProducto'   => '',
            'optionsMonth'          => $getOptions['Month'],
            'optionsYear'           => $getOptions['Year']
        ));
    }
    public function getDataRentabilidadSMSCliente(Request $request)
    {
        if($request->selectedClientes){
            $dataCostoSMSCliente =
                DB::select('
                SELECT m.fecha, sum(m.cantidad) cantidad, 
                       round(sum(m.cantidad*m.precio_costo), 2) compra,
                       round(sum(m.cantidad*m.precio_venta), 2) venta,
                       round(sum(m.cantidad*(m.precio_venta - m.precio_costo)), 2) rentabilidad
                  FROM movestadistica m , clientes c
                  WHERE m.id_cliente = c.id_cliente AND
                        m.id_producto = 28 AND
                        m.anio = '.$request->selectedYear.' AND
                        m.mes = '.$request->selectedMonth.' AND
                        m.id_cliente = '.$request->selectedClientes.' 
                GROUP BY m.fecha
                ORDER BY m.fecha;
            ');
        }else{
            $dataCostoSMSCliente =
                DB::select('
                 SELECT m.fecha, 
                       round(sum(m.cantidad*m.precio_costo), 2) compra,
                       round(sum(m.cantidad*m.precio_venta), 2) venta,
                       round(sum(m.cantidad*(m.precio_venta - m.precio_costo)), 2) rentabilidad
                  FROM movestadistica m, clientes c
                  WHERE m.id_cliente = c.id_cliente AND
                        m.id_producto = 28 AND
                        m.anio = '.$request->selectedYear.' AND
                        m.mes = '.$request->selectedMonth.'
                GROUP BY m.fecha
                ORDER BY m.fecha;
            ');
        }

        $dataLabel = collect($dataCostoSMSCliente)->map(function ($item, $key) {
            return Carbon::parse($item->fecha)->format('j');
        })->unique()->values()->toArray();

        $labeldataSets = collect($dataCostoSMSCliente)->map(function ($item, $key) {
            return $item->rentabilidad;
        })->values()->toArray();

        $dataTable = $this->chartBarTable($dataLabel, $labeldataSets);

        $chart = Charts::create('area', 'chartjs')
            ->title("Rentabilidad Diaria de SMS por Cliente")
            ->dimensions(0, 400)
            ->colors($this->arrayColors())
            ->elementLabel('Total Rentabilidad')
            ->labels($dataLabel)
            ->values($labeldataSets);

        return response()->json([
            'chartHTML' => $chart->html(),
            'chartScript' => $chart->script(),
            'chartTable' => $dataTable
        ]);
    }

    public function getOptions()
    {
        $clientes = Clientes::Select()
            ->get()
            ->toArray();

        $month = Meses::Select()
            ->get()
            ->toArray();

        $year = Anios::Select()
            ->get()
            ->toArray();

        $typeproduct = TipoProductos::Select()
            ->get()
            ->toArray();

        $proveedores = Proveedores::Select()
            ->get()
            ->toArray();

        $options['Clientes'] = $clientes;
        $options['Month'] = $month;
        $options['Year'] = $year;
        $options['TipoProducto'] = $typeproduct;
        $options['Proveedores'] = $proveedores;

        return $options;
    }
}
