<?php

namespace Securitec\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends SecuritecController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['user', 'admin']);
        return view('plataforma')->with(array(
            'filterReport'    => false
        ));
    }
}
