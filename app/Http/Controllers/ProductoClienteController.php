<?php

namespace Securitec\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Securitec\Anios;
use Securitec\Clientes;
use Securitec\Meses;
use Securitec\Productos;
use Securitec\ProductosCliente;

class ProductoClienteController extends SecuritecController
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $response = $request->user()->authorizeRoles(['admin']);
        $getOptions = $this->getOptions();
        
        
        if($response) return view('elements/productos/productosclientes')->with(array(
            'titleModule'           => 'Lista de Productos Clientes',
            'filterReport'          => true,
            'titleFilterReport'     => 'Productos Clientes',
            'viewDateMonth'         => true,
            'viewDateYear'          => true,
            'viewClientes'          => true,
            'viewProveedores'       => false,
            'viewTipoProducto'      => false,
            'viewRangeDateAndMonth' => false,
            'optionsClientes'       => $getOptions['Clientes'],
            'optionsProveedores'    => '',
            'optionsTipoProducto'   => '',
            'optionsMonth'          => $getOptions['Month'],
            'optionsYear'           => $getOptions['Year']
        ));

        return view('errors/autorizacion');
    }

    public function listProductosClientes(Request $request)
    {
        //dd($request);
        if ($request->isMethod('post')) {
            $query_producto_cliente_list    = $this->productos_clientes_list_query($request->selectedClientes, $request->selectedMonth, $request->selectedYear);
            $builderview                    = $this->builderview($query_producto_cliente_list);
            $outgoingcollection             = $this->outgoingcollection($builderview);
            $list_productos_clientes    = $this->FormatDatatable($outgoingcollection);
            return $list_productos_clientes;
        }
    }

    protected function productos_clientes_list_query($idCliente, $dateMonth, $dateYear)
    {
        //dd($idCliente);
        $productos_clientes_list_query = ProductosCliente::Select()
            ->with('cliente')
            ->with('producto')
            ->where('id_cliente', $idCliente)
            ->where('anio', $dateYear)
            ->where('mes', $dateMonth)
            ->get()
            ->toArray();
        
        

        return $productos_clientes_list_query;
    }

    protected function builderview($producto_cliente_list_query,$type='')
    {
        $posicion = 0;
        $idList = 0;
        foreach ($producto_cliente_list_query as $query) {

            $idList ++;
            $builderview[$posicion]['id']                   = $idList;
            $builderview[$posicion]['id_producto_cliente']  = $query['id'];
            $builderview[$posicion]['cliente']              = ucwords(Str::lower($query['cliente']['cliente']));
            $builderview[$posicion]['producto']             = ucwords(Str::lower($query['producto']['producto']));
            $builderview[$posicion]['cantidad']             = $query['cantidad'];
            $builderview[$posicion]['precio_venta']         = number_format($query['precio_venta'], 4, '.', ',');
            $posicion ++;
        }
        if(!isset($builderview)){
            $builderview = [];
        }
        return $builderview;
    }

    protected function outgoingcollection($builderview)
    {
        $outgoingcollection = new \Illuminate\Support\Collection;
        foreach ($builderview as $view) {
            $outgoingcollection->push([
                'id'                => $view['id'],
                'cliente'           => $view['cliente'],
                'producto'          => $view['producto'],
                'cantidad'          => $view['cantidad'],
                'precio_venta'      => $view['precio_venta'],
                'action'            => '<span data-toggle="tooltip" data-placement="left" title="Editar Producto Cliente"><a class="btn btn-warning btn-xs" onclick="responseModal('."'div.dialogSecuritec','formProductosClientes','".$view['id_producto_cliente']."'".')" data-toggle="modal" data-target="#modalSecuritec"><i class="fa fa-edit" aria-hidden="true"></i></a></span>',
            ]);
        }
        return $outgoingcollection;
    }

    public function formProductosClientes(Request $request)
    {
        $response = $request->user()->authorizeRoles(['admin']);
        if($response) {
            if($request->valueID == null){
                $idProductoCliente = ProductosCliente::max('id');
                $getOptions = $this->getOptions();
                return view('elements/productos/form/formClientesProductos')->with(array(
                    'idProductoCliente'     => ($idProductoCliente ? $idProductoCliente + 1:1),
                    'idEstado'              => '',
                    'optionsClientes'       => $getOptions['Clientes'],
                    'optionsProductos'      => $getOptions['Productos'],
                    'optionsMonth'          => $getOptions['Month'],
                    'optionsYear'           => $getOptions['Year'],
                    'selectedClientes'      => '',
                    'selectedProductos'     => '',
                    'selectedMonth'         => '',
                    'selectedYear'          => '',
                    'numCantidad'           => '',
                    'numPrecioVenta'        => '',
                    'updateForm'            => false
                ));
            }else{
                $getProductoCliente = $this->getProductoCliente($request->valueID);
                $getOptions = $this->getOptions();
                return view('elements/productos/form/formClientesProductos')->with(array(
                    'idProductoCliente'     => $request->valueID,
                    'idEstado'              => $getProductoCliente[0]['id_estado'],
                    'optionsClientes'       => $getOptions['Clientes'],
                    'optionsProductos'      => $getOptions['Productos'],
                    'optionsMonth'          => $getOptions['Month'],
                    'optionsYear'           => $getOptions['Year'],
                    'selectedClientes'      => $getProductoCliente[0]['id_cliente'],
                    'selectedProductos'     => $getProductoCliente[0]['id_producto'],
                    'selectedMonth'         => $getProductoCliente[0]['mes'],
                    'selectedYear'          => $getProductoCliente[0]['anio'],
                    'numCantidad'           => $getProductoCliente[0]['cantidad'],
                    'numPrecioVenta'        => $getProductoCliente[0]['precio_venta'],
                    'updateForm'            => true
                ));
            }

        }
        return view('errors/autorizacion');
    }

    public function getOptions()
    {
        $clientes = Clientes::Select()
            ->get()
            ->toArray();

        $productos = Productos::Select()
            ->get()
            ->toArray();

        $month = Meses::Select()
            ->get()
            ->toArray();

        $year = Anios::Select()
            ->get()
            ->toArray();

        $options['Clientes'] = $clientes;
        $options['Productos'] = $productos;
        $options['Month'] = $month;
        $options['Year'] = $year;

        return $options;
    }

    public function getProductoCliente($idProductoCliente)
    {
        $productoCliente = ProductosCliente::Select()
                                ->where('id', $idProductoCliente)
                                ->get()
                                ->toArray();
        return $productoCliente;
    }

    public function saveFormProductosClientes(Request $request)
    {
        if ($request->isMethod('post')) {
            try{
                DB::beginTransaction();
                ProductosCliente::updateOrCreate([
                    'id'            => $request->productoClienteID
                ], [
                    'id_cliente'    => $request->selectedCliente,
                    'id_producto'   => $request->selectedProducto,
                    'anio'          => $request->selectedAnio,
                    'mes'           => $request->selectedMonth,
                    'cantidad'      => $request->numCantidad,
                    'precio_venta'  => $request->numPrecioVenta,
                    'id_estado'     => $request->estadoID
                ]);


                /*$request->productoClienteID ? DB::select('CALL sp_inserta_movestadistica_cliente ("'.$request->selectedAnio.'","'.$request->selectedMonth.'","'.$request->selectedCliente.'","'.$request->selectedProducto.'","'.$request->numCantidad.'","'.$request->numPrecioVenta.'")') : '';*/
                DB::commit();

                $action = ($request->productoClienteID ? 'Actualizo' : 'Creo');
                return ['message' => 'Success', 'action' => $action];
            } catch(\Exception $e){
                DB::rollback();
                return ['message' => 'Error'];
            }
        }
        return ['message' => 'Error'];
    }
}
