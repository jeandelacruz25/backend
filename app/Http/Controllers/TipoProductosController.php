<?php

namespace Securitec\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Securitec\TipoProductos;

class TipoProductosController extends SecuritecController
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $response = $request->user()->authorizeRoles(['admin']);
        if($response) return view('elements/productos/tiposproductos')->with(array(
            'titleModule'   => 'Lista de Tipos de Producto',
            'filterReport'  => false
        ));
        return view('errors/autorizacion');
    }

    public function listTipoProductos(Request $request)
    {
        if ($request->isMethod('post')) {
            $query_tipo_producto_list       = $this->tipos_productos_list_query();
            $builderview                    = $this->builderview($query_tipo_producto_list);
            $outgoingcollection             = $this->outgoingcollection($builderview);
            $list_tipos_productos                 = $this->FormatDatatable($outgoingcollection);
            return $list_tipos_productos;
        }
    }

    protected function tipos_productos_list_query()
    {
        $tipos_productos_list_query = TipoProductos::Select()
            ->get()
            ->toArray();
        return $tipos_productos_list_query;
    }

    protected function builderview($producto_list_query,$type='')
    {
        $posicion = 0;
        $idList = 0;
        foreach ($producto_list_query as $query) {

            $idList ++;
            $builderview[$posicion]['id']                = $idList;
            $builderview[$posicion]['id_tipoproducto']   = $query['id_tipoproducto'];
            $builderview[$posicion]['tipo_producto']     = ucwords(Str::lower($query['tipo_producto']));
            $posicion ++;
        }
        if(!isset($builderview)){
            $builderview = [];
        }
        return $builderview;
    }

    protected function outgoingcollection($builderview)
    {
        $outgoingcollection = new \Illuminate\Support\Collection;
        foreach ($builderview as $view) {
            $outgoingcollection->push([
                'id'             => $view['id'],
                'tipo_producto'  => $view['tipo_producto'],
                'action'         => '<span data-toggle="tooltip" data-placement="left" title="Editar Tipo Producto"><a class="btn btn-warning btn-xs" onclick="responseModal('."'div.dialogSecuritec','formTipoProductos','".$view['id_tipoproducto']."'".')" data-toggle="modal" data-target="#modalSecuritec"><i class="fa fa-edit" aria-hidden="true"></i></a></span>',
            ]);
        }
        return $outgoingcollection;
    }

    public function formTipoProductos(Request $request)
    {
        $response = $request->user()->authorizeRoles(['admin']);
        if($response) {
            if($request->valueID == null){
                $idTipoProducto = TipoProductos::max('id_tipoproducto');
                return view('elements/productos/form/formTipoProductos')->with(array(
                    'idTipoProducto'            => ($idTipoProducto ? $idTipoProducto + 1:1),
                    'nameTipoProducto'          => '',
                    'updateForm'                => false
                ));
            }else{
                $getTipoProducto = $this->getTipoProducto($request->valueID);
                return view('elements/productos/form/formTipoProductos')->with(array(
                    'idTipoProducto'            => $request->valueID,
                    'nameTipoProducto'          => $getTipoProducto[0]['tipo_producto'],
                    'updateForm'                => true
                ));
            }

        }
        return view('errors/autorizacion');
    }

    public function getTipoProducto($idTipoProducto)
    {
        $tipo_producto = TipoProductos::Select()
                    ->where('id_tipoproducto', $idTipoProducto)
                    ->get()
                    ->toArray();

        return $tipo_producto;
    }

    public function saveFormTipoProductos(Request $request)
    {
        if ($request->isMethod('post')) {
            $tipoProductoQuery = TipoProductos::updateOrCreate([
                'id_tipoproducto'       => $request->tipoProductoID
            ], [
                'tipo_producto'         => $request->nameTipoProducto
            ]);
            $action = ($request->tipoProductoID ? 'Actualizo' : 'Creo');
            if ($tipoProductoQuery) {
                return ['message' => 'Success', 'action' => $action];
            }
            return ['message' => 'Error'];
        }
        return ['message' => 'Error'];
    }
}
