<?php

namespace Securitec\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Securitec\Http\Requests\ProductosRequest;
use Securitec\Productos;
use Securitec\TipoProductos;

class ProductosController extends SecuritecController
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $response = $request->user()->authorizeRoles(['admin']);
        if($response) return view('elements/productos/index')->with(array(
            'titleModule'   => 'Lista de Productos',
            'filterReport'  => false
        ));
        return view('errors/autorizacion');
    }

    public function listProductos(Request $request)
    {
        if ($request->isMethod('post')) {
            $query_producto_list        = $this->productos_list_query();
            $builderview                = $this->builderview($query_producto_list);
            $outgoingcollection         = $this->outgoingcollection($builderview);
            $list_productos             = $this->FormatDatatable($outgoingcollection);
            return $list_productos;
        }
    }

    protected function productos_list_query()
    {
        $productos_list_query = Productos::Select()
            ->with('tipo_producto')
            ->get()
            ->toArray();
        return $productos_list_query;
    }

    protected function builderview($producto_list_query,$type='')
    {
        $posicion = 0;
        $idList = 0;
        foreach ($producto_list_query as $query) {

            $idList ++;
            $builderview[$posicion]['id']                   = $idList;
            $builderview[$posicion]['id_producto']          = $query['id_producto'];
            $builderview[$posicion]['id_tipoproducto']      = $query['id_tipoproducto'];
            $builderview[$posicion]['tipo_producto']        = $query['tipo_producto']['tipo_producto'];
            $builderview[$posicion]['producto']             = ucwords(Str::lower($query['producto']));
            $posicion ++;
        }
        if(!isset($builderview)){
            $builderview = [];
        }
        return $builderview;
    }

    protected function outgoingcollection($builderview)
    {
        $outgoingcollection = new \Illuminate\Support\Collection;
        foreach ($builderview as $view) {
            $outgoingcollection->push([
                'id'             => $view['id'],
                'producto'       => $view['producto'],
                'tipo_producto'  => $view['tipo_producto'],
                'action'         => '<span data-toggle="tooltip" data-placement="left" title="Editar Producto"><a class="btn btn-warning btn-xs" onclick="responseModal('."'div.dialogSecuritec','formProductos','".$view['id_producto']."'".')" data-toggle="modal" data-target="#modalSecuritec"><i class="fa fa-edit" aria-hidden="true"></i></a></span>',
            ]);
        }
        return $outgoingcollection;
    }

    public function formProductos(Request $request)
    {
        $response = $request->user()->authorizeRoles(['admin']);
        if($response) {
            if($request->valueID == null){
                $idProducto = Productos::max('id_producto');
                $getTipoProducto = $this->getTipoProducto();
                return view('elements/productos/form/formProductos')->with(array(
                    'idProducto'            => ($idProducto ? $idProducto + 1:1),
                    'nameProducto'          => '',
                    'optionsTipoProducto'   => $getTipoProducto,
                    'selectedTipoProducto'  => '',
                    'updateForm'            => false
                ));
            }else{
                $getProducto = $this->getProducto($request->valueID);
                $getTipoProducto = $this->getTipoProducto();
                return view('elements/productos/form/formProductos')->with(array(
                    'idProducto'            => $request->valueID,
                    'nameProducto'          => $getProducto[0]['producto'],
                    'optionsTipoProducto'   => $getTipoProducto,
                    'selectedTipoProducto'  => $getProducto[0]['id_tipoproducto'],
                    'updateForm'            => true
                ));
            }

        }
        return view('errors/autorizacion');
    }

    public function getProducto($idProducto)
    {
        $producto = Productos::Select()
                        ->with('tipo_producto')
                        ->where('id_producto', $idProducto)
                        ->get()
                        ->toArray();

        return $producto;
    }

    public function getTipoProducto()
    {
        $tipo_producto = TipoProductos::Select()
                            ->get()
                            ->toArray();

        return $tipo_producto;
    }

    public function saveFormProductos(ProductosRequest $request)
    {
        if ($request->isMethod('post')) {
            $productoQuery = Productos::updateOrCreate([
                'id_producto'       => $request->productoID
            ], [
                'id_tipoproducto'   => $request->selectedTipoProducto,
                'producto'          => $request->nameProducto
            ]);
            $action = ($request->productoID ? 'Actualizo' : 'Creo');
            if ($productoQuery) {
                return ['message' => 'Success', 'action' => $action];
            }
            return ['message' => 'Error'];
        }
        return ['message' => 'Error'];
    }
}
