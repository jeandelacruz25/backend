<?php

namespace Securitec\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Securitec\Clientes;
use Securitec\Http\Requests\ClientesRequest;

class ClientesController extends SecuritecController
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $response = $request->user()->authorizeRoles(['admin']);
        if($response) return view('elements/clientes/index')->with(array(
            'titleModule'   => 'Lista de Clientes',
            'filterReport'  => false
        ));
        return view('errors/autorizacion');
    }

    public function listClientes(Request $request)
    {
        if ($request->isMethod('post')) {
            $query_user_list        = $this->clientes_list_query();
            $builderview            = $this->builderview($query_user_list);
            $outgoingcollection     = $this->outgoingcollection($builderview);
            $list_users             = $this->FormatDatatable($outgoingcollection);
            return $list_users;
        }
    }

    protected function clientes_list_query()
    {
        $clientes_list_query = Clientes::Select()
            ->get()
            ->toArray();
        return $clientes_list_query;
    }

    protected function builderview($user_list_query,$type='')
    {
        $posicion = 0;
        $idList = 0;
        foreach ($user_list_query as $query) {
            $idList ++;
            $builderview[$posicion]['id']           = $idList;
            $builderview[$posicion]['id_cliente']   = $query['id_cliente'];
            $builderview[$posicion]['cliente']      = ucwords(Str::lower($query['cliente']));
            $posicion ++;
        }
        if(!isset($builderview)){
            $builderview = [];
        }
        return $builderview;
    }

    protected function outgoingcollection($builderview)
    {
        $outgoingcollection = new \Illuminate\Support\Collection;
        foreach ($builderview as $view) {
            $outgoingcollection->push([
                'id'            => $view['id'],
                'cliente'       => $view['cliente'],
                'action'        => '<span data-toggle="tooltip" data-placement="left" title="Editar Cliente"><a class="btn btn-warning btn-xs" onclick="responseModal('."'div.dialogSecuritec','formClientes','".$view['id_cliente']."'".')" data-toggle="modal" data-target="#modalSecuritec"><i class="fa fa-edit" aria-hidden="true"></i></a></span>',
            ]);
        }
        return $outgoingcollection;
    }

    public function formClientes(Request $request)
    {
        $response = $request->user()->authorizeRoles(['admin']);
        if($response) {
            if($request->valueID == null){
                $idCliente = Clientes::max('id_cliente');
                return view('elements/clientes/form/formClientes')->with(array(
                    'idCliente'      => ($idCliente ? $idCliente + 1:1),
                    'nameCliente'    => '',
                    'updateForm'     => false
                ));
            }else{
                $getCliente = $this->getCliente($request->valueID);
                return view('elements/clientes/form/formClientes')->with(array(
                    'idCliente'      => $request->valueID,
                    'nameCliente'    => $getCliente[0]['cliente'],
                    'updateForm'     => true
                ));
            }

        }
        return view('errors/autorizacion');
    }

    public function getCliente($idCliente)
    {
        $clientes = Clientes::Select()
            ->where('id_cliente', $idCliente)
            ->get()
            ->toArray();
        return $clientes;
    }

    public function saveFormCliente(ClientesRequest $request)
    {
        if ($request->isMethod('post')) {
            $clienteQuery = Clientes::updateOrCreate([
                'id_cliente' => $request->clienteID
            ], [
                'cliente'    => $request->nameCliente
            ]);
            $action = ($request->clienteID ? 'Actualizo' : 'Creo');
            if ($clienteQuery) {
                return ['message' => 'Success', 'action' => $action];
            }
            return ['message' => 'Error'];
        }
        return ['message' => 'Error'];
    }

}
