<?php

namespace Securitec\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Securitec\Proveedores;

class ProveedoresController extends SecuritecController
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $response = $request->user()->authorizeRoles(['admin']);
        if($response) return view('elements/proveedores/index')->with(array(
            'titleModule'   => 'Lista de Proveedores',
            'filterReport'  => false
        ));
        return view('errors/autorizacion');
    }

    public function listProveedores(Request $request)
    {
        if ($request->isMethod('post')) {
            $query_user_list        = $this->proveedores_list_query();
            $builderview            = $this->builderview($query_user_list);
            $outgoingcollection     = $this->outgoingcollection($builderview);
            $list_users             = $this->FormatDatatable($outgoingcollection);
            return $list_users;
        }
    }

    protected function proveedores_list_query()
    {
        $proveedores_list_query = Proveedores::Select()
            ->get()
            ->toArray();
        return $proveedores_list_query;
    }

    protected function builderview($user_list_query,$type='')
    {
        $posicion = 0;
        $idList = 0;
        foreach ($user_list_query as $query) {
            $idList ++;
            $builderview[$posicion]['id']             = $idList;
            $builderview[$posicion]['id_proveedor']   = $query['id_proveedor'];
            $builderview[$posicion]['proveedor']      = ucwords(Str::lower($query['proveedor']));
            $posicion ++;
        }
        if(!isset($builderview)){
            $builderview = [];
        }
        return $builderview;
    }

    protected function outgoingcollection($builderview)
    {
        $outgoingcollection = new \Illuminate\Support\Collection;
        foreach ($builderview as $view) {
            $outgoingcollection->push([
                'id'            => $view['id'],
                'proveedor'     => $view['proveedor'],
                'action'        => '<span data-toggle="tooltip" data-placement="left" title="Editar Proveedor"><a class="btn btn-warning btn-xs" onclick="responseModal('."'div.dialogSecuritec','formProveedores','".$view['id_proveedor']."'".')" data-toggle="modal" data-target="#modalSecuritec"><i class="fa fa-edit" aria-hidden="true"></i></a></span>',
            ]);
        }
        return $outgoingcollection;
    }

    public function formProveedores(Request $request)
    {
        $response = $request->user()->authorizeRoles(['admin']);
        if($response) {
            if($request->valueID == null){
                $idProveedor = Proveedores::max('id_proveedor');
                return view('elements/proveedores/form/formProveedores')->with(array(
                    'idProveedor'      => ($idProveedor ? $idProveedor + 1:1),
                    'nameProveedor'    => '',
                    'updateForm'       => false
                ));
            }else{
                $getProovedor = $this->getProveedor($request->valueID);
                return view('elements/proveedores/form/formProveedores')->with(array(
                    'idProveedor'      => $request->valueID,
                    'nameProveedor'    => $getProovedor[0]['proveedor'],
                    'updateForm'       => true
                ));
            }

        }
        return view('errors/autorizacion');
    }

    public function getProveedor($idProveedor)
    {
        $proovedores = Proveedores::Select()
            ->where('id_proveedor', $idProveedor)
            ->get()
            ->toArray();
        return $proovedores;
    }

    public function saveFormProveedor(Request $request)
    {
        if ($request->isMethod('post')) {
            $proveedorQuery = Proveedores::updateOrCreate([
                'id_proveedor' => $request->proveedorID
            ], [
                'proveedor'    => $request->nameProveedor
            ]);
            $action = ($request->proveedorID ? 'Actualizo' : 'Creo');
            if ($proveedorQuery) {
                return ['message' => 'Success', 'action' => $action];
            }
            return ['message' => 'Error'];
        }
        return ['message' => 'Error'];
    }
}
