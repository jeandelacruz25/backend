<?php

namespace Securitec\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nameCliente'                => 'required|min:1|max:100'
        ];
    }

    public function messages()
    {
        return [
            'nameCliente.required'       => 'Debes ingresar un nombre de un cliente',
            'nameCliente.min'            => 'El nombre del cliente, puede tener minimo 1 caracter',
            'nameCliente.max'            => 'El nombre del cliente, debe tener maximo 100 caracteres'
        ];
    }
}
