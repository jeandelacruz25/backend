<?php

namespace Securitec\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'selectedTipoProducto'        => 'required',
            'nameProducto'                => 'required|min:1|max:200'
        ];
    }
    public function messages()
    {
        return [
            'selectedTipoProducto.required'     => 'Debes seleccionar un tipo de producto',
            'nameProducto.required'             => 'Debes ingresar un nombre de un cliente',
            'nameProducto.min'                  => 'El nombre del cliente, puede tener minimo 1 caracter',
            'nameProducto.max'                  => 'El nombre del cliente, debe tener maximo 200 caracteres'
        ];
    }
}
