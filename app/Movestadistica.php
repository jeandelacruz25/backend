<?php

namespace Securitec;

use Illuminate\Database\Eloquent\Model;

class Movestadistica extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'movestadistica';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;
}
