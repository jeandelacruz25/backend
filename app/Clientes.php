<?php

namespace Securitec;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'clientes';
    protected $primaryKey   = 'id_cliente';
    public    $timestamps   = false;

    protected $fillable = [
        'id_cliente', 'cliente',
    ];
}
