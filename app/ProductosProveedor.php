<?php

namespace Securitec;

use Illuminate\Database\Eloquent\Model;

class ProductosProveedor extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'productos_proveedor';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;

    protected $fillable = [
        'id', 'id_proveedor', 'id_producto', 'anio', 'mes', 'cantidad', 'precio_costo', 'id_estado',
    ];

    public function proveedor(){
        return $this->belongsTo('Securitec\Proveedores','id_proveedor');
    }

    public function producto(){
        return $this->belongsTo('Securitec\Productos','id_producto');
    }
}
