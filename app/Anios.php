<?php

namespace Securitec;

use Illuminate\Database\Eloquent\Model;

class Anios extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'anios';
    protected $primaryKey   = 'anio';
    public    $timestamps   = false;

    protected $fillable = [
        'anio',
    ];
}