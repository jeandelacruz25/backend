<?php

namespace Securitec;

use Illuminate\Database\Eloquent\Model;

class Estadisticas extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'estadistica';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;
}
