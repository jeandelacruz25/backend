<?php

namespace Securitec;

use Illuminate\Database\Eloquent\Model;

class TipoProductos extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'tipoproductos';
    protected $primaryKey   = 'id_tipoproducto';
    public    $timestamps   = false;

    protected $fillable = [
        'id_tipoproducto', 'tipo_producto',
    ];
}
