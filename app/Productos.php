<?php

namespace Securitec;

use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'productos';
    protected $primaryKey   = 'id_producto';
    public    $timestamps   = false;

    protected $fillable = [
        'id_producto', 'id_tipoproducto', 'producto',
    ];

    public function tipo_producto(){
        return $this->belongsTo('Securitec\TipoProductos','id_tipoproducto');
    }
}
