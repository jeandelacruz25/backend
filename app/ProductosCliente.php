<?php

namespace Securitec;

use Illuminate\Database\Eloquent\Model;

class ProductosCliente extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'productos_cliente';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;

    protected $fillable = [
        'id', 'id_cliente', 'id_producto', 'anio', 'mes', 'cantidad', 'precio_venta', 'id_estado',
    ];

    public function cliente(){
        return $this->belongsTo('Securitec\Clientes','id_cliente');
    }

    public function producto(){
        return $this->belongsTo('Securitec\Productos','id_producto');
    }
}
