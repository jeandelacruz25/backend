<?php

namespace Securitec;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'roles';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;


    public function users()
	{
	    return $this
	        ->belongsToMany('Securitec\User')
	        ->withTimestamps();
	}
}
