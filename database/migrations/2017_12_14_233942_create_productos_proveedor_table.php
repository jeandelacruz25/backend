<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosProveedorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos_proveedor', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_proveedor')->nullable();
            $table->integer('id_producto')->nullable();
            $table->char('anio',4)->nullable();
            $table->char('mes',2)->nullable();
            $table->integer('cantidad')->default(0)->nullable();
            $table->float('precio_costo',18,4)->nullable();

            $table->foreign('id_proveedor')
                ->references('id_proveedor')
                ->on('proveedores');
            $table->foreign('id_producto')
                ->references('id_producto')
                ->on('productos');
            $table->charset = 'latin1';
            $table->collation  = 'latin1_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos_proveedor');
    }
}
