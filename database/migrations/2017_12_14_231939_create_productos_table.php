<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->integer('id_producto')->primary();
            $table->integer('id_tipoproducto');
            $table->string('producto',200);
            $table->foreign('id_tipoproducto')
                ->references('id_tipoproducto')
                ->on('tipo_productos');
            $table->charset = 'latin1';
            $table->collation  = 'latin1_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
