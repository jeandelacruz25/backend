<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovestadisticaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movestadistica', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha')->nullable();
            $table->char('anio',4)->nullable();
            $table->char('mes',2)->nullable();
            $table->integer('id_producto')->nullable();
            $table->integer('id_tipoproducto')->nullable();
            $table->integer('id_cliente')->nullable();
            $table->integer('id_proveedor')->nullable();
            $table->float('cantidad',18,4)->nullable();
            $table->float('precio_costo',18,8)->nullable();
            $table->float('precio_venta',18,8)->nullable();
            $table->float('rentabilidad',18,8)->nullable();
            $table->foreign('id_producto')
                ->references('id_producto')
                ->on('productos');
            $table->foreign('id_tipoproducto')
                ->references('id_tipoproducto')
                ->on('tipo_productos');
            $table->foreign('id_cliente')
                ->references('id_cliente')
                ->on('clientes');
            $table->foreign('id_proveedor')
                ->references('id_proveedor')
                ->on('proveedores');
            $table->charset = 'latin1';
            $table->collation  = 'latin1_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movestadistica');
    }
}
