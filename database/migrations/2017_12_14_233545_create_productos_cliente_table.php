<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosClienteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos_cliente', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_cliente')->nullable();
            $table->integer('id_producto')->nullable();
            $table->char('anio',4)->nullable();
            $table->char('mes',2)->nullable();
            $table->integer('cantidad')->default(0)->nullable();
            $table->float('precio_venta',18,4)->nullable();

            $table->foreign('id_cliente')
                ->references('id_cliente')
                ->on('clientes');
            $table->foreign('id_producto')
                ->references('id_producto')
                ->on('productos');
            $table->charset = 'latin1';
            $table->collation  = 'latin1_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos_cliente');
    }
}
