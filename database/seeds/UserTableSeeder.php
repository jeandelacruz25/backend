<?php

use Illuminate\Database\Seeder;
use Securitec\User;
use Securitec\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_user = Role::where('name', 'user')->first();
        $role_admin = Role::where('name', 'admin')->first();
        $user = new User();
        $user->name = 'User';
        $user->username = 'test';
        $user->email = 'user@example.com';
        $user->password = bcrypt('12345');
        $user->save();

        $user->roles()->attach($role_user);
        $user = new User();
        $user->name = 'Admin';
        $user->username = 'admin';
        $user->email = 'admin@example.com';
        $user->password = bcrypt('12345');
        $user->save();
        $user->roles()->attach($role_admin);
    }
}
