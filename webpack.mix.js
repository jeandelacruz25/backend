const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.sass('resources/assets/sass/app.scss', 'public/css');

mix.styles([
    'resources/assets/securitec/css/securitec.css',
    'node_modules/datatables.net-bs/css/dataTables.bootstrap.css',
    'node_modules/vue2-animate/dist/vue2-animate.css'
], 'public/css/securitec.css').version();

mix.js([
    'resources/assets/js/app.js'
], 'public/js/app.js').version();

mix .babel([
        'node_modules/datatables.net-bs/js/dataTables.bootstrap.js'
    ], 'public/js/plugins.js').version()

    .babel([
        'resources/assets/securitec/js/helper.js',
        'resources/assets/securitec/js/datatables.js'
    ], 'public/js/helper.js').version();

mix
    .styles([
        'resources/assets/vendor/adminlte/plugins/bootstrap/dist/css/bootstrap.css',
        'resources/assets/vendor/adminlte/plugins/font-awesome/css/font-awesome.css',
        'resources/assets/vendor/adminlte/plugins/Ionicons/css/ionicons.css',
        'resources/assets/vendor/adminlte/dist/css/AdminLTE.css',
        'resources/assets/securitec/css/skin-red.css'
    ],'public/css/adminlte.css').version()
    .scripts([
        'resources/assets/vendor/adminlte/plugins/bootstrap/dist/js/bootstrap.js',
        'resources/assets/vendor/adminlte/plugins/fastclick/lib/fastclick.js',
        'resources/assets/vendor/adminlte/dist/js/adminlte.js'
    ],'public/js/adminlte.js').version();

mix.js([
    'resources/assets/securitec/js/vue/vueSecuritec.js'
], 'public/js/vue/vueSecuritec.js').version();

mix.copy('node_modules/jquery.backstretch/jquery.backstretch.js', 'public/js')
    .copy('resources/assets/securitec/favicon.png', 'public/favicon.png')
    .copy('resources/assets/img', 'public/img')
    .copy('resources/assets/vendor/adminlte/dist/img', 'public/dist/img')
    .copy('resources/assets/vendor/adminlte/plugins/bootstrap/dist/fonts'       , 'public/fonts')
    .copy('resources/assets/vendor/adminlte/plugins/Ionicons/fonts'             , 'public/fonts')
    .copy('resources/assets/vendor/adminlte/plugins/font-awesome/fonts'         , 'public/fonts')
    .copy('resources/assets/securitec/js/form'                                  , 'public/js/form');

mix.copy('node_modules/daterangepicker/daterangepicker.js'      ,'public/plugins')
    .copy('node_modules/daterangepicker/daterangepicker.css'    ,'public/plugins');